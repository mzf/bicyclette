/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/
#include "testframework.h"

#include "../vst/vstparameter.h"

int TestVSTParameters()
{
    TESTS_START

    //objects for test
    VSTParameter vstParam(0, "abcd");
    VSTParameter vstParamLabel(0, "efgh", "ijkl");

    //---------------------------------------------------------
    TEST_TITLE("GetName()")
    char textResult[10];
    vstParam.GetName(textResult);
    if( (textResult[0] != 'a') ||
        (textResult[1] != 'b') ||
        (textResult[2] != 'c') ||
        (textResult[3] != 'd') ||
        (textResult[4] != 0))
    {
        TEST_FAILED("The parameter name is incorrect.")
    }

    vstParamLabel.GetName(textResult);
    if( (textResult[0] != 'e') ||
        (textResult[1] != 'f') ||
        (textResult[2] != 'g') ||
        (textResult[3] != 'h') ||
        (textResult[4] != 0))
    {
        TEST_FAILED("The parameter name (with label) is incorrect.")
    }

    //---------------------------------------------------------
    TEST_TITLE("GetLabel()")
    vstParam.GetLabel(textResult);
    if(textResult[0] != 0)
    {
        TEST_FAILED("Default label is incorrect.")
    }

    vstParamLabel.GetLabel(textResult);
    if( (textResult[0] != 'i') ||
        (textResult[1] != 'j') ||
        (textResult[2] != 'k') ||
        (textResult[3] != 'l') ||
        (textResult[4] != 0))
    {
        TEST_FAILED("The label is incorrect.")
    }

    //---------------------------------------------------------
    TEST_TITLE("GetDisplayValue()")
    char displayText[9]; //kVstMaxParamStrLen+1

    vstParam.GetDisplayValue(0.,displayText);
    if( (displayText[0] != '0') ||
        (displayText[1] != '.') ||
        (displayText[2] != '0') ||
        (displayText[3] != '0') ||
        (displayText[4] != '0') ||
        (displayText[5] != '0') ||
        (displayText[6] != '0') ||
        (displayText[7] != '0') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue 0. is incorrect.")
    }

    vstParam.GetDisplayValue(0.123456,displayText);
    if((displayText[0] != '0') ||
        (displayText[1] != '.') ||
        (displayText[2] != '1') ||
        (displayText[3] != '2') ||
        (displayText[4] != '3') ||
        (displayText[5] != '4') ||
        (displayText[6] != '5') ||
        (displayText[7] != '6') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue 0.123456 is incorrect.")
    }

    vstParam.GetDisplayValue(0.12345678,displayText);
    if((displayText[0] != '0') ||
        (displayText[1] != '.') ||
        (displayText[2] != '1') ||
        (displayText[3] != '2') ||
        (displayText[4] != '3') ||
        (displayText[5] != '4') ||
        (displayText[6] != '5') ||
        (displayText[7] != '6') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue 0.12345678 is incorrect.")
    }

    vstParam.GetDisplayValue(-0.5,displayText);
    if((displayText[0] != '-') ||
        (displayText[1] != '0') ||
        (displayText[2] != '.') ||
        (displayText[3] != '5') ||
        (displayText[4] != '0') ||
        (displayText[5] != '0') ||
        (displayText[6] != '0') ||
        (displayText[7] != '0') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue -0.5 is incorrect.")
    }

    vstParam.GetDisplayValue(-1.,displayText);
    if((displayText[0] != '-') ||
        (displayText[1] != '1') ||
        (displayText[2] != '.') ||
        (displayText[3] != '0') ||
        (displayText[4] != '0') ||
        (displayText[5] != '0') ||
        (displayText[6] != '0') ||
        (displayText[7] != '0') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue -1. is incorrect.")
    }

    vstParam.GetDisplayValue(-1.123456,displayText);
    if((displayText[0] != '-') ||
        (displayText[1] != '1') ||
        (displayText[2] != '.') ||
        (displayText[3] != '1') ||
        (displayText[4] != '2') ||
        (displayText[5] != '3') ||
        (displayText[6] != '4') ||
        (displayText[7] != '5') ||
        (displayText[8] != 0) )
    {
        TEST_FAILED("displayValue -1.123456 is incorrect.")
    }

    vstParam.GetDisplayValue(12345678.,displayText);
    if((displayText[0] != 'H') ||
        (displayText[1] != 'u') ||
        (displayText[2] != 'g') ||
        (displayText[3] != 'e') ||
        (displayText[4] != '!') ||
        (displayText[5] != 0))
    {
        TEST_FAILED("displayValue 12345678. is incorrect.")
    }

    vstParam.GetDisplayValue(-1234567.,displayText);
    if((displayText[0] != 'H') ||
        (displayText[1] != 'u') ||
        (displayText[2] != 'g') ||
        (displayText[3] != 'e') ||
        (displayText[4] != '!') ||
        (displayText[5] != 0))
    {
        TEST_FAILED("displayValue -1234567. is incorrect.")
    }


    TESTS_END
}
