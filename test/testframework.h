/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _TEST_FRAMEWORK_H_
#define _TEST_FRAMEWORK_H_

#include <iostream>

//---------------------------------------------------------
// Minimal testing framework
//---------------------------------------------------------
#define TESTS_START             int numFailed = 0;

#define TEST_TITLE(title)       std::cout << "* " << title << std::endl;

#define TEST_FAILED(message)    numFailed++;\
                                std::cout << "Failed: " << message << std::endl;

#define TESTS_END               return numFailed;

#endif // _TEST_FRAMEWORK_H_
