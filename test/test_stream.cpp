/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/
#include "testframework.h"

#include "../tools/Stream.h"

bool IsDoubleEqual(double value1, double value2)
{
    double tolerance = 1e-6;
    return (value1 < value2 + tolerance) && (value1 > value2 - tolerance);
}

int TestStream()
{
    TESTS_START

    //---------------------------------------------------------
    TEST_TITLE("Write/Read char")
    {
        Stream stream;
        char value1 = 22;
        char value2 = 0;
        char value3 = 255;
        stream.Write(value1);
        stream.Write(value2);
        stream.Write(value3);

        char result;
        stream.Read(result);
        if(result != value1)
        {
            TEST_FAILED("bad char value1.")
        }
        stream.Read(result);
        if(result != value2)
        {
            TEST_FAILED("bad char value2.")
        }
        stream.Read(result);
        if(result != value3)
        {
            TEST_FAILED("bad char value3.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Write/Read int")
    {
        Stream stream;
        int value1 = 15;
        int value2 = 0;
        int value3 = -12;
        stream.Write(value1);
        stream.Write(value2);
        stream.Write(value3);

        int result;
        stream.Read(result);
        if(result != value1)
        {
            TEST_FAILED("bad integer value1.")
        }
        stream.Read(result);
        if(result != value2)
        {
            TEST_FAILED("bad integer value2.")
        }
        stream.Read(result);
        if(result != value3)
        {
            TEST_FAILED("bad integer value3.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Write/Read bool")
    {
        Stream stream;
        bool value1 = true;
        bool value2 = true;
        bool value3 = false;
        stream.Write(value1);
        stream.Write(value2);
        stream.Write(value3);

        bool result;
        stream.Read(result);
        if(result != value1)
        {
            TEST_FAILED("bad bool value1.")
        }
        stream.Read(result);
        if(result != value2)
        {
            TEST_FAILED("bad bool value2.")
        }
        stream.Read(result);
        if(result != value3)
        {
            TEST_FAILED("bad bool value3.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Write/Read unsigned int")
    {
        Stream stream;
        unsigned int value1 = 15;
        unsigned int value2 = 12;
        stream.Write(value1);
        stream.Write(value2);

        unsigned int result;
        stream.Read(result);
        if(result != value1)
        {
            TEST_FAILED("bad unsigned integer value1.")
        }
        stream.Read(result);
        if(result != value2)
        {
            TEST_FAILED("bad unsigned integer value2.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Write/Read unsigned long")
    {
        Stream stream;
        unsigned long value1 = 15;
        unsigned long value2 = 12;
        stream.Write(value1);
        stream.Write(value2);

        unsigned long result;
        stream.Read(result);
        if(result != value1)
        {
            TEST_FAILED("bad unsigned long value1.")
        }
        stream.Read(result);
        if(result != value2)
        {
            TEST_FAILED("bad unsigned long value2.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Write/Read double")
    {
        Stream stream;
        double value1 = 15.0;
        double value2 = 0.0;
        double value3 = -12.0;
        double value4 = 1.0001;
        double value5 = -1.12345;
        stream.Write(value1);
        stream.Write(value2);
        stream.Write(value3);
        stream.Write(value4);
        stream.Write(value5);

        double result;
        stream.Read(result);
        if(IsDoubleEqual(result, value1) == false)
        {
            TEST_FAILED("bad double value1.")
        }
        stream.Read(result);
        if(IsDoubleEqual(result, value2) == false)
        {
            TEST_FAILED("bad double value2.")
        }
        stream.Read(result);
        if(IsDoubleEqual(result, value3) == false)
        {
            TEST_FAILED("bad double value3.")
        }
        stream.Read(result);
        if(IsDoubleEqual(result, value4) == false)
        {
            TEST_FAILED("bad double value4.")
        }
        stream.Read(result);
        if(IsDoubleEqual(result, value5) == false)
        {
            TEST_FAILED("bad double value5.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Bad Read")
    {
        Stream stream;
        char result = 5;
        stream.Read(result);
        if(result != 0)
        {
            TEST_FAILED("char \"result\" must equals 0.")
        }
    }
    {
        Stream stream;
        int result = 5;
        stream.Read(result);
        if(result != 0)
        {
            TEST_FAILED("int \"result\" must equals 0.")
        }
    }
    {
        Stream stream;
        bool result = true;
        stream.Read(result);
        if(result != false)
        {
            TEST_FAILED("bool \"result\" must equals false.")
        }
    }
    {
        Stream stream;
        unsigned int result = 5;
        stream.Read(result);
        if(result != 0)
        {
            TEST_FAILED("unsigned int \"result\" must equals 0.")
        }
    }
    {
        Stream stream;
        unsigned long result = 5;
        stream.Read(result);
        if(result != 0)
        {
            TEST_FAILED("unsigned long \"result\" must equals 0.")
        }
    }
    {
        Stream stream;
        double result = 5;
        stream.Read(result);
        if(IsDoubleEqual(result, 0.) == false)
        {
            TEST_FAILED("double \"result\" must equals 0.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Clear")
    {
        Stream stream;
        stream.Write(1);
        stream.Write(2);
        stream.Write(3);
        stream.Clear();
        stream.Write(4);
        int result;
        stream.Read(result);
        if(result != 4)
        {
            TEST_FAILED("Stream not cleared.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Init")
    {
        int buffer[] = {1,2,3,4};
        Stream stream;
        stream.Init(buffer, 4*sizeof(int));
        int result1, result2, result3, result4;
        stream.Read(result1);
        stream.Read(result2);
        stream.Read(result3);
        stream.Read(result4);
        if((result1 != 1) ||
           (result2 != 2) ||
           (result3 != 3) ||
           (result4 != 4))
        {
            TEST_FAILED("Stream not initialized.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("GetData")
    {
        Stream stream;
        stream.Write(1);
        stream.Write(2);
        stream.Write(3);
        stream.Write(4);
        void* buffer = NULL;
        unsigned int bufferSize = 0;
        stream.GetData(&buffer, &bufferSize);
        if(buffer == NULL)
        {
            TEST_FAILED("Buffer is NULL.");
        }
        if(bufferSize != 4*sizeof(int))
        {
            TEST_FAILED("Bad buffer size.")
        }
        int* intBuffer = static_cast<int*>(buffer);
        if((intBuffer[0] != 1) ||
           (intBuffer[1] != 2) ||
           (intBuffer[2] != 3) ||
           (intBuffer[3] != 4))
        {
            TEST_FAILED("Bad buffer data.")
        }

    }


    TESTS_END
}
