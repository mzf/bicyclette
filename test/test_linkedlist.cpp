/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/
#include "testframework.h"

//---------------------------------------------------------
// Tests of the Linked List with the BicycletteEvent class
//---------------------------------------------------------
#include "../synth/BicycletteEvent.h"
#include "../tools/LinkedList.h"
typedef LinkedList<BicycletteEvent> BicycletteEvents;

int TestBicycletteEvents()
{
    TESTS_START

    //---------------------------------------------------------
    TEST_TITLE("Create")
    BicycletteEvents list;
    if(list.Begin() != NULL)
    {
        TEST_FAILED("The first element is not empty.")
    }
    if(list.Next() != NULL)
    {
        TEST_FAILED("The next element is not empty.")
    }
    list.DeleteCurrent();
    if(list.Begin() != NULL)
    {
        TEST_FAILED("After DeleteCurrent() on empty list, the first element is not empty.")
    }
    if(list.Next() != NULL)
    {
        TEST_FAILED("After DeleteCurrent() on empty list, the next element is not empty.")
    }

    //---------------------------------------------------------
    TEST_TITLE("Add One")
    {
        BicycletteEvent be;
        be.channel = -1;
        list.Add(be);

        BicycletteEvent* begin = list.Begin();
        if(begin == 0)
        {
            TEST_FAILED("After Add the list is empty.")
        }
        else if(begin->channel != -1)
        {
            TEST_FAILED("The added element is not the first.")
        }

        BicycletteEvent* next = list.Next();
        if(next != 0)
        {
            TEST_FAILED("After added one, there is more than one element in the list.")
        }

        list.Begin();
        list.DeleteCurrent();
        if(list.Begin() != NULL)
        {
            TEST_FAILED("After DeleteCurrent() on one element list, the first element is not empty.")
        }
        if(list.Next() != NULL)
        {
            TEST_FAILED("After DeleteCurrent() on one element list, the next element is not empty.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Add 10")
    for(int i = 0; i<10; i++)
    {
        BicycletteEvent be;
        be.channel = i;
        list.Add(be);
    }

    //---------------------------------------------------------
    TEST_TITLE("Access 10")
    {
        BicycletteEvent* ev = list.Begin();
        for(int i = 0; i<10; i++)
        {
            if(ev == 0)
            {
                TEST_FAILED("After Add 10 there is not 10 elements in the list.")
            }
            else if(ev->channel != i)
            {
                TEST_FAILED("After Add 10 the elements are not in the right order.")
            }
            ev = list.Next();
        }
        if(ev != 0)
        {
            TEST_FAILED("After Add 10 there is more than 10 elements in the list.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Access 5 and start again")
    {
        BicycletteEvent* ev = list.Begin();
        BicycletteEvent* begin = ev;
        for(int i = 0; i<5; i++)
        {
            if(ev == 0)
            {
                TEST_FAILED("Unexpected end of list.")
            }
            else
            {
                ev = list.Next();
            }
        }
        ev = list.Begin();
        if(ev->channel != begin->channel)
        {
            TEST_FAILED("After read 5 and restart, the first element changed.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Delete in the middle of the list")
    {
        BicycletteEvent* ev = list.Begin();
        for(int i = 0; i<5; i++)
        {
            if(ev == 0)
            {
                TEST_FAILED("Unexpected end of list.")
            }
            else
            {
                ev = list.Next();
            }
        }
        list.DeleteCurrent();

        int count = 0;
        ev = list.Begin();
        while(ev != 0)
        {
            count++;
            ev = list.Next();
        }
        if(count != 9)
        {
            TEST_FAILED("There is not 9 elements in the list.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Delete first element")
    {
        BicycletteEvent* ev = list.Begin();
        list.DeleteCurrent();

        int count = 0;
        ev = list.Begin();
        while(ev != 0)
        {
            count++;
            ev = list.Next();
        }
        if(count != 8)
        {
            TEST_FAILED("There is not 8 elements in the list.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Delete last element")
    {
        BicycletteEvent* ev = list.Begin();
        for(int i = 0; i<7; i++)
        {
            if(ev == 0)
            {
                TEST_FAILED("Unexpected end of list.")
            }
            else
            {
                ev = list.Next();
            }
        }
        list.DeleteCurrent();

        int count = 0;
        ev = list.Begin();
        while(ev != 0)
        {
            count++;
            ev = list.Next();
        }
        if(count != 7)
        {
            TEST_FAILED("There is not 7 elements in the list.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Add at the end")
    {
        for(int i = 0; i<10; i++)
        {
            BicycletteEvent be;
            be.channel = 100 + i;
            list.Add(be);
        }

        int count = 0;
        BicycletteEvent* ev = list.Begin();
        while(ev != 0)
        {
            count++;
            ev = list.Next();
        }
        if(count != 17)
        {
            TEST_FAILED("There is not 17 elements in the list.")
        }

        ev = list.Begin();
        int values[17] = {1,2,3,4,6,7,8,100,101,102,103,104,105,106,107,108,109};
        for(int i = 0; i<17; i++)
        {
            if(ev == 0)
            {
                TEST_FAILED("Unexpected end of list.")
            }
            else if(ev->channel != values[i])
            {
                TEST_FAILED("The elements are not in the expected order.")
            }
            ev = list.Next();
        }
        if(ev != 0)
        {
            TEST_FAILED("There is more than 17 elements in the list.")
        }

    }

    //---------------------------------------------------------
    TEST_TITLE("Delete All elements")
    list.DeleteAll();
    BicycletteEvent* be = list.Begin();
    if(be != 0)
    {
        TEST_FAILED("After DeleteAll there is still an element in the list.")
    }

    //---------------------------------------------------------
    TEST_TITLE("Delete 10, one by one")
    {
        for(int i = 0; i<10; i++)
        {
            BicycletteEvent be;
            be.channel = i;
            list.Add(be);
        }
        int count = 0;
        BicycletteEvent* be = list.Begin();
        while(be != 0)
        {
            list.DeleteCurrent();
            count++;
            be = list.Next();
        }

        if(count != 10)
        {
            TEST_FAILED("We do not delete 10 elements.")
        }

        be = list.Begin();
        if(be != 0)
        {
            TEST_FAILED("After Delete there is still an element in the list.")
        }
    }

    TESTS_END
}
