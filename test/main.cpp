/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

//-------------------------------------------------------------------
// This program tests some class of the bicyclette project
//-------------------------------------------------------------------

#include <iostream>
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

//-------------------------------------------------------------------
// Test functions
//-------------------------------------------------------------------
int TestBicycletteEvents();
int TestVSTParameters();
int TestStream();
int TestSerialization();

//-------------------------------------------------------------------
// Tests launcher
//-------------------------------------------------------------------
int main(void)
{
    std::cout << "------------------------------------------------" << std::endl;
    std::cout << "| Unit tests of some bicyclette synth objects. |" << std::endl;
    std::cout << "------------------------------------------------" << std::endl;
    std::cout << std::endl;

    int totalFailed = 0;
    int localFailed;


    std::cout << "BicycletteEvents (Linked List)" << std::endl;
    std::cout << "==============================" << std::endl;

    localFailed = TestBicycletteEvents();

    totalFailed += localFailed;
    std::cout << std::endl;
    std::cout << localFailed << " test(s) failed" << std::endl;
    std::cout << std::endl;



    std::cout << "VST Parameters" << std::endl;
    std::cout << "==============================" << std::endl;

    localFailed = TestVSTParameters();

    totalFailed += localFailed;
    std::cout << std::endl;
    std::cout << localFailed << " test(s) failed" << std::endl;
    std::cout << std::endl;

    std::cout << "Stream" << std::endl;
    std::cout << "==============================" << std::endl;

    localFailed = TestStream();

    totalFailed += localFailed;
    std::cout << std::endl;
    std::cout << localFailed << " test(s) failed" << std::endl;
    std::cout << std::endl;

    std::cout << "Serialization" << std::endl;
    std::cout << "==============================" << std::endl;

    localFailed = TestSerialization();

    totalFailed += localFailed;
    std::cout << std::endl;
    std::cout << localFailed << " test(s) failed" << std::endl;
    std::cout << std::endl;



    std::cout << "Total: " << totalFailed << " test(s) failed" << std::endl;

    if(totalFailed > 0)
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
