/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/
#include "testframework.h"

#include "../synth/BicycletteSynth.h"
#include "../synth/SynthParameters.h"
#include "../synth/Channel.h"
#include "../synth/FilterHighPass.h"
#include "../synth/EnvADSR.h"
#include "../synth/OscNoise.h"
#include "../synth/OscSaw.h"
#include "../synth/OscSinus.h"
#include "../synth/OscSquare.h"

void PrintStreamSize(Stream &stream)
{
    //Information
    void* data = NULL;
    unsigned int size = 0;
    stream.GetData(&data, &size);
    std::cout << "  [stream size: " << size << "]" << std::endl;
}

int TestSerialization()
{
    TESTS_START

    //---------------------------------------------------------
    TEST_TITLE("BicycletteSynth")
    {

        //create and init synth
        double referenceSampleRate = 100.;
        unsigned int referenceInputChannelNumber = 3;
        unsigned int referenceOutputChannelNumber = 4;
        unsigned int referenceCurrentChannel = 2;

        BicycletteSynth synth(referenceSampleRate, referenceInputChannelNumber, referenceOutputChannelNumber);
        synth.SetParameter(SynthParameters_currentChannel, referenceCurrentChannel);

        //serialize
        Stream saveStream;
        synth.Serialize(saveStream);

        //Information
        PrintStreamSize(saveStream);

        //create an other synth
        BicycletteSynth newSynth(200.,6,13);

        //deserialize
        newSynth.Deserialize(saveStream);

        //check state of the synth
        double sampleRate = newSynth.GetParameter(SynthParameters_sampleRate);
        if(sampleRate != referenceSampleRate)
        {
            TEST_FAILED("'sampleRate' failed to de/serialize.")
        }

        unsigned int inputChannelNumber = newSynth.GetParameter(SynthParameters_inputChannelsNumber);
        if(inputChannelNumber != referenceInputChannelNumber)
        {
            TEST_FAILED("'inputChannelNumber' failed to de/serialize.")
        }

        unsigned int outputChannelNumber = newSynth.GetParameter(SynthParameters_outputChannelsNumber);
        if(outputChannelNumber != referenceOutputChannelNumber)
        {
            TEST_FAILED("'outputChannelNumber' failed to de/serialize.")
        }

        unsigned int currentChannel = newSynth.GetParameter(SynthParameters_currentChannel);
        if(currentChannel != referenceCurrentChannel)
        {
            TEST_FAILED("'currentChannel' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Channel")
    {

        //create a Channel object
        Channel channel1;
        double channelPanning = -0.7;
        double channelVolume = 0.3;
        double channelSampleRate = 100.0;
        double filterType = static_cast<double>(FilterType_HighPass);
        double activation = static_cast<double>(true);
        channel1.SetParameter(SynthParameters_ChannelPanning, channelPanning);
        channel1.SetParameter(SynthParameters_ChannelVolume, channelVolume);
        channel1.SetParameter(SynthParameters_sampleRate, channelSampleRate);
        channel1.SetParameter(SynthParameters_FilterType, filterType);
        channel1.SetParameter(SynthParameters_FilterActivation, activation);

        //serialize it
        Stream channelStream;
        channelStream.Clear();
        channel1.Serialize(channelStream);

        //Information
        PrintStreamSize(channelStream);

        Channel channel2;
        channel2.Deserialize(channelStream);

        double result;
        result = channel2.GetParameter(SynthParameters_ChannelPanning);
        if(result != channelPanning)
        {
            TEST_FAILED("'panning' failed to de/serialize.")
        }

        result = channel2.GetParameter(SynthParameters_ChannelVolume);
        if(result != channelVolume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }

        result = channel2.GetParameter(SynthParameters_sampleRate);
        if(result != channelSampleRate)
        {
            TEST_FAILED("'samplerate' failed to de/serialize.")
        }

        result = channel2.GetParameter(SynthParameters_FilterType);
        if(result != filterType)
        {
            TEST_FAILED("'filterType' failed to de/serialize.")
        }

        result = channel2.GetParameter(SynthParameters_FilterActivation);
        if(result != activation)
        {
            TEST_FAILED("'activation' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Filter")
    {
        //create a filter object
        Filter* filter1 = new FilterHighPass();
        double sampleRate = 110.;
        double frequency = 300.;
        double quality = 0.5;
        filter1->SetParameter(SynthParameters_sampleRate, sampleRate);
        filter1->SetParameter(SynthParameters_FilterFrequency, frequency);
        filter1->SetParameter(SynthParameters_FilterQuality, quality);

        //serialize it
        Stream filterStream;
        filterStream.Clear();
        filter1->Serialize(filterStream);

        //Information
        PrintStreamSize(filterStream);

        Filter* filter2 = new FilterHighPass();
        filter2->Deserialize(filterStream);

        double result;
        result = filter2->GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'samplerate' failed to de/serialize.")
        }

        result = filter2->GetParameter(SynthParameters_FilterFrequency);
        if(result != frequency)
        {
            TEST_FAILED("'frequency' failed to de/serialize.")
        }

        result = filter2->GetParameter(SynthParameters_FilterQuality);
        if(result != quality)
        {
            TEST_FAILED("'quality' failed to de/serialize.")
        }

        delete filter1; filter1 = NULL;
        delete filter2; filter2 = NULL;
    }

    //---------------------------------------------------------
    TEST_TITLE("Note")
    {
        //create a note object
        Note note1;
        int midiNoteIndex = 5;
        double volume = 0.67;
        double sampleRate = 678.;
        unsigned int currentOscIndex = 1;
        int oscillatorsModulationType = OscillatorModulation_FM;

        note1.Activate(midiNoteIndex, volume);
        note1.SetParameter(SynthParameters_sampleRate, sampleRate);
        note1.SetParameter(SynthParameters_OscCurrent, currentOscIndex);
        note1.SetParameter(SynthParameters_Note_OscModType, currentOscIndex);

        //serialize it
        Stream stream;
        stream.Clear();
        note1.Serialize(stream);

        //Information
        PrintStreamSize(stream);

        Note note2;
        note2.Deserialize(stream);

        double result;
        result = note2.GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'samplerate' failed to de/serialize.")
        }
        int noteIndex = note2.GetNoteIndex();
        if(noteIndex != midiNoteIndex)
        {
            TEST_FAILED("'midiNoteIndex' failed to de/serialize.")
        }
        if(note2.IsActive() == false)
        {
            TEST_FAILED("'active' failed to de/serialize.")
        }
        result = note2.GetVolume();
        if(result != volume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
        result = note2.GetParameter(SynthParameters_OscCurrent);
        if(static_cast<unsigned int>(result) != currentOscIndex)
        {
            TEST_FAILED("'currentOscIndex' failed to de/serialize.")
        }
        result = note2.GetParameter(SynthParameters_Note_OscModType);
        if(static_cast<int>(result) != oscillatorsModulationType)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("Envelope")
    {
        //create a note object
        double sampleRate = 678.;
        Envelope* env1 = new EnvADSR(sampleRate);
        double attackTime = 1.;
        double attackLevel = 0.45;
        double decayTime = 2.;
        double sustainLevel = 0.67;
        double releaseTime = 0.4;

        env1->KeyPressed(); //set status to Attack
        env1->SetParameter(SynthParameters_EnvADSR_attackLevel, attackLevel);
        env1->SetParameter(SynthParameters_EnvADSR_attackTime, attackTime);
        env1->SetParameter(SynthParameters_EnvADSR_decayTime, decayTime);
        env1->SetParameter(SynthParameters_EnvADSR_releaseTime, releaseTime);
        env1->SetParameter(SynthParameters_EnvADSR_sustainLevel, sustainLevel);

        //serialize it
        Stream stream;
        stream.Clear();
        env1->Serialize(stream);

        //Information
        PrintStreamSize(stream);

        Envelope* env2 = new EnvADSR(100.);
        env2->Deserialize(stream);

        double result;
        result = env2->GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'samplerate' failed to de/serialize.")
        }
        result = env2->GetParameter(SynthParameters_EnvADSR_attackLevel);
        if(result != attackLevel)
        {
            TEST_FAILED("'attackLevel' failed to de/serialize.")
        }
        result = env2->GetParameter(SynthParameters_EnvADSR_attackTime);
        if(result != attackTime)
        {
            TEST_FAILED("'attackTime' failed to de/serialize.")
        }
        result = env2->GetParameter(SynthParameters_EnvADSR_decayTime);
        if(result != decayTime)
        {
            TEST_FAILED("'decayTime' failed to de/serialize.")
        }
        result = env2->GetParameter(SynthParameters_EnvADSR_releaseTime);
        if(result != releaseTime)
        {
            TEST_FAILED("'releaseTime' failed to de/serialize.")
        }
        result = env2->GetParameter(SynthParameters_EnvADSR_sustainLevel);
        if(result != sustainLevel)
        {
            TEST_FAILED("'sustainLevel' failed to de/serialize.")
        }
        if(env2->IsActive() == false)
        {
            TEST_FAILED("'active' failed to de/serialize.")
        }

        delete env1; env1 = NULL;
        delete env2; env2 = NULL;
    }

    //---------------------------------------------------------
    TEST_TITLE("OscNoise")
    {
        OscNoise osc1;
        double oscVolume = 0.4;
        osc1.SetParameter(SynthParameters_OscVolume, oscVolume);
        Stream stream;
        stream.Clear();
        osc1.Serialize(stream);

        //Information
        PrintStreamSize(stream);

        OscNoise osc2;
        osc2.Deserialize(stream);
        double volume = osc2.GetParameter(SynthParameters_OscVolume);
        if(volume != oscVolume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("OscSaw")
    {
        OscSaw osc1;
        double volume = 0.4;
        double frequency = 200.;
        //double phase = 0.3;
        double sampleRate = 600.;
        double FMAmount = 0.2;
        osc1.SetParameter(SynthParameters_OscVolume, volume);
        osc1.SetParameter(SynthParameters_OscFrequency, frequency);
        osc1.SetParameter(SynthParameters_sampleRate, sampleRate);
        osc1.SetParameter(SynthParameters_OscFMInput, FMAmount);

        Stream stream;
        stream.Clear();
        osc1.Serialize(stream);

        //Information
        PrintStreamSize(stream);

        OscSaw osc2;
        osc2.Deserialize(stream);

        double result = osc2.GetParameter(SynthParameters_OscVolume);
        if(result != volume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFrequency);
        if(result != frequency)
        {
            TEST_FAILED("'frequency' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'sampleRate' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFMInput);
        if(result != FMAmount)
        {
            TEST_FAILED("'FMAmount' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("OscSinus")
    {
        OscSinus osc1;
        double volume = 0.4;
        double frequency = 200.;
        //double phase = 0.3;
        double sampleRate = 600.;
        double FMAmount = 0.2;
        osc1.SetParameter(SynthParameters_OscVolume, volume);
        osc1.SetParameter(SynthParameters_OscFrequency, frequency);
        osc1.SetParameter(SynthParameters_sampleRate, sampleRate);
        osc1.SetParameter(SynthParameters_OscFMInput, FMAmount);

        Stream stream;
        stream.Clear();
        osc1.Serialize(stream);

        //Information
        PrintStreamSize(stream);

        OscSinus osc2;
        osc2.Deserialize(stream);

        double result = osc2.GetParameter(SynthParameters_OscVolume);
        if(result != volume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFrequency);
        if(result != frequency)
        {
            TEST_FAILED("'frequency' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'sampleRate' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFMInput);
        if(result != FMAmount)
        {
            TEST_FAILED("'FMAmount' failed to de/serialize.")
        }
    }

    //---------------------------------------------------------
    TEST_TITLE("OscSquare")
    {
        OscSquare osc1;
        double volume = 0.4;
        double frequency = 200.;
        //double phase = 0.3;
        double sampleRate = 600.;
        double FMAmount = 0.2;
        double pwmFactor = 0.7;
        osc1.SetParameter(SynthParameters_OscVolume, volume);
        osc1.SetParameter(SynthParameters_OscFrequency, frequency);
        osc1.SetParameter(SynthParameters_sampleRate, sampleRate);
        osc1.SetParameter(SynthParameters_OscFMInput, FMAmount);
        osc1.SetParameter(SynthParameters_OscSquare_pwmFactor, pwmFactor);

        Stream stream;
        stream.Clear();
        osc1.Serialize(stream);

        //Information
        PrintStreamSize(stream);

        OscSquare osc2;
        osc2.Deserialize(stream);

        double result = osc2.GetParameter(SynthParameters_OscVolume);
        if(result != volume)
        {
            TEST_FAILED("'volume' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFrequency);
        if(result != frequency)
        {
            TEST_FAILED("'frequency' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_sampleRate);
        if(result != sampleRate)
        {
            TEST_FAILED("'sampleRate' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscFMInput);
        if(result != FMAmount)
        {
            TEST_FAILED("'FMAmount' failed to de/serialize.")
        }
        result = osc2.GetParameter(SynthParameters_OscSquare_pwmFactor);
        if(result != pwmFactor)
        {
            TEST_FAILED("'pwmFactor' failed to de/serialize.")
        }
    }

    TESTS_END
}
