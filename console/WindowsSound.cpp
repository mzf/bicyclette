/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

//#include <dsound.h>
#include "wine_dsound.h"
#include <math.h>

#include "WindowsSound.h"

//#include <wincon.h> //for GetConsoleWindow()

#include "constants.h"

WindowsSound::WindowsSound(Renderer* renderer)
{
    this->directSoundDevice = NULL;
    this->directSoundBuffer = NULL;
    this->renderer = renderer;
    this->writePosition = 0;
}

WindowsSound::~WindowsSound(void)
{
    this->directSoundBuffer->Release();
    this->directSoundDevice->Release();
}

bool WindowsSound::Init()
{
    HRESULT hr;

    //create the sound device
    hr = DirectSoundCreate8(NULL, &(this->directSoundDevice), NULL);
    if(FAILED(hr))
        return false;

    //set cooperative level to PRIORITY (=high)
    HWND hwndC = GetConsoleWindow() ;
    hr = this->directSoundDevice->SetCooperativeLevel(hwndC, DSSCL_PRIORITY);
    if(FAILED(hr))
        return false;

    //create an output wave format
    WAVEFORMATEX waveFormat;
    memset(&waveFormat, 0, sizeof(WAVEFORMATEX));
    waveFormat.wFormatTag = WAVE_FORMAT_PCM;
    waveFormat.nChannels = CHANNELS_NUMBER;
    waveFormat.nSamplesPerSec = SAMPLE_RATE;
    waveFormat.wBitsPerSample = BITS_PER_SAMPLE;
    waveFormat.nBlockAlign = BLOCK_ALIGN;
    waveFormat.nAvgBytesPerSec = AVERAGE_BYTES_PER_SECOND; //msdn recommendation for PCM format

    //Primary sound buffer
    DSBUFFERDESC bufferDesc;
    memset(&bufferDesc, 0, sizeof(DSBUFFERDESC));
    bufferDesc.dwSize = sizeof(DSBUFFERDESC);
    bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER|DSBCAPS_STICKYFOCUS;
    bufferDesc.dwBufferBytes = 0;
    bufferDesc.lpwfxFormat = NULL;

    LPDIRECTSOUNDBUFFER primaryDirectSoundBuffer;
    hr = this->directSoundDevice->CreateSoundBuffer(&bufferDesc, &primaryDirectSoundBuffer, NULL);
    if(FAILED(hr))
        return false;
    hr = primaryDirectSoundBuffer->SetFormat(&waveFormat);
    if(FAILED(hr))
        return false;

    //set up a buffer description
    DSBUFFERDESC soundBufferDescription;
    memset(&soundBufferDescription, 0, sizeof(DSBUFFERDESC));
    soundBufferDescription.dwSize = sizeof(DSBUFFERDESC);
    //DSBCAPS_STICKYFOCUS: allows to play sound even if we don't have have focus.
    //DSBCAPS_GETCURRENTPOSITION2: tells DirectSound we'll use the GetPosition method later on that sound buffer.
    soundBufferDescription.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_STICKYFOCUS;
    soundBufferDescription.dwBufferBytes = BUFFER_LENGTH_IN_BYTES;
    soundBufferDescription.lpwfxFormat = &waveFormat;

    //create the sound buffer
    hr = this->directSoundDevice->CreateSoundBuffer(&soundBufferDescription, &(this->directSoundBuffer), NULL);
    if(FAILED(hr))
        return false;

    return true;

}

bool WindowsSound::Start()
{
    HRESULT hr = E_FAIL;

    if(this->directSoundBuffer)
    {
        hr = this->directSoundBuffer->Play(0, 0, DSBPLAY_LOOPING);
    }

    return SUCCEEDED(hr);
}

bool WindowsSound::Stop()
{
    if(this->directSoundBuffer)
    {
        HRESULT hr = this->directSoundBuffer->Stop();
        if(FAILED(hr))
            return false;
    }

    return true;
}

void WindowsSound::Update()
{

    DWORD playCursor;
    DWORD writeLength = 0;
    HRESULT hr;

    //TODO: check for directSoundBuffer validity

    hr = this->directSoundBuffer->GetCurrentPosition(&playCursor, NULL);
    if (hr != DS_OK)
        playCursor = 0;

    if (this->writePosition < playCursor)
    {
        writeLength = playCursor - this->writePosition;
    }
    else if(this->writePosition > playCursor)
    {
        writeLength = BUFFER_LENGTH_IN_BYTES - (this->writePosition - playCursor);
    }
    else //m_writePosition == playCursor
    {
        DWORD status;
        this->directSoundBuffer->GetStatus(&status);
        if((playCursor == 0) &&
           ((status&DSBSTATUS_PLAYING) == DSBSTATUS_PLAYING))
        {
            //first pass, so fill the entire buffer
            writeLength = BUFFER_LENGTH_IN_BYTES;
        }
        else
        {
            writeLength = 0;
        }
    }

    LPVOID	p1,p2;
    DWORD	l1,l2;
    hr = this->directSoundBuffer->Lock(this->writePosition, writeLength, &p1, &l1, &p2, &l2,0);
    if ( hr != DS_OK)
    {
        //error
        this->directSoundBuffer->Restore();
        this->directSoundBuffer->Play(0, 0, DSBPLAY_LOOPING);
    }

    //render sound
    if ((p1) && (l1>0))
        RenderPCM16(p1,l1);
	if ((p2) && (l2>0))
        RenderPCM16(p2,l2);

    this->directSoundBuffer->Unlock(p1,l1,p2,l2);

    //Update circular buffer
    this->writePosition += writeLength;
    if (this->writePosition >= BUFFER_LENGTH_IN_BYTES)
    {
        this->writePosition -= BUFFER_LENGTH_IN_BYTES;
    }
}

void WindowsSound::RenderPCM16(void *buffer, unsigned long bufferLengthInBytes)
{
    if(buffer == NULL)
        return;

    //cast the buffer to PCM 16 bits = 2 bytes
    short* pcm16Buffer = (short*)buffer;

    //bufferLength is in bytes, we need a counter by sample (each 2 bytes)
    unsigned long samplesNumber = bufferLengthInBytes/(BYTES_PER_SAMPLE*CHANNELS_NUMBER);

    //ask the synth to render the requested samples
    double** synthBuffer = new double*[CHANNELS_NUMBER];
    for(unsigned int i=0;i<CHANNELS_NUMBER;i++)
    {
        double* channel = new double[samplesNumber];
        synthBuffer[i] = channel;
    }
    this->renderer->Render(synthBuffer, samplesNumber);

    for(unsigned long i=0; i<samplesNumber; i++)
    {
        for(unsigned int channelIndex = 0; channelIndex< CHANNELS_NUMBER; channelIndex++)
        {
            double val = synthBuffer[channelIndex][i];
            //scale it to PCM 16bit: -32767 to +32767
            short pcmVal = short(val*32767.);
            //interlaced PCM [left,right|left,right|left,right|...]
            pcm16Buffer[(i*CHANNELS_NUMBER)+channelIndex] = pcmVal;
        }
    }

    //delete synth buffers
    for(unsigned int i=0;i<CHANNELS_NUMBER;i++)
    {
        delete[] synthBuffer[i];
    }
    delete[] synthBuffer;
}
