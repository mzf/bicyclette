/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_


//Output sound format for windows renderer
#define SAMPLE_RATE 44100
#define BITS_PER_SAMPLE 16
#define CHANNELS_NUMBER 2

//#define SAMPLE_RATE_DOUBLE SAMPLE_RATE ## .0
#define CONCAT(a,b) CONCAT_1(a,b)
#define CONCAT_1(a,b) CONCAT_2(a,b)
#define CONCAT_2(a,b) a##b
#define SAMPLE_RATE_DOUBLE CONCAT(SAMPLE_RATE,.0)

//msdn recommendation for PCM format:
#define BLOCK_ALIGN ((CHANNELS_NUMBER * BITS_PER_SAMPLE)/8) //4
#define AVERAGE_BYTES_PER_SECOND (SAMPLE_RATE * BLOCK_ALIGN) //176400

#define BUFFER_LENGTH_IN_BYTES (AVERAGE_BYTES_PER_SECOND) // 1 second
#define BYTES_PER_SAMPLE (BITS_PER_SAMPLE/8) //2

#endif //_CONSTANTS_H_
