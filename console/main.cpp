/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "..\synth\BicycletteSynth.h"
#include "WindowsSound.h"
#include "constants.h"
#include <time.h>

void sleep(clock_t milliseconds)
{
    clock_t target = milliseconds + clock();
    while (target > clock());
}

int main()
{
    //create a synth
    BicycletteSynth synth(SAMPLE_RATE_DOUBLE, 16, CHANNELS_NUMBER);

    //create a sound manager
    WindowsSound windowsSound(&synth);

    if(windowsSound.Init() == false)
    {
        //TODO: handle error
    }

    if(windowsSound.Start() == false)
    {
        //TODO: handle error
    }

    for(int i=0; i<50; i++)
    {
        if(i == 0)
        {
            BicycletteEvent ev;
            ev.channel = 1;
            ev.delta = 50;
            ev.note = 69;
            ev.type = BicycletteEventType_NoteOn;
            ev.velocity = 100;
            synth.AddEvent(ev);
        }

        if(i == 40)
        {
            BicycletteEvent ev;
            ev.channel = 1;
            ev.delta = 50;
            ev.note = 69;
            ev.type = BicycletteEventType_NoteOff;
            ev.velocity = 100;
            synth.AddEvent(ev);
        }

        windowsSound.Update();
        sleep(100);
    }

    windowsSound.Stop();
    return 0;
}
