/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _WINDOWSSOUND_H_
#define _WINDOWSSOUND_H_

//#include <dsound.h>
#include "wine_dsound.h"
#include "..\synth\renderer.h"

//This class manages the windows sound system via directsound

class WindowsSound
{
public:
    WindowsSound(Renderer* renderer);
    ~WindowsSound(void);

    bool Init();
    bool Start();
    void Update();
    bool Stop();

private:
    void RenderPCM16(void *buffer, unsigned long bufferLengthInBytes);

    LPDIRECTSOUND8      directSoundDevice;
    LPDIRECTSOUNDBUFFER directSoundBuffer;
    Renderer*           renderer;
    DWORD               writePosition;
};

#endif //_WINDOWSSOUND_H_

