/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _MIDI_H_
#define _MIDI_H_


//-----------------------------------------------------------------------------------------
// Midi masks to decode Midi messages
// http://www.midi.org/techspecs/midimessages.php
//-----------------------------------------------------------------------------------------

#define MIDI_MASK_CHANNEL   0x0f
#define MIDI_MASK_STATUS    0xf0

#define MIDI_MASK_NOTEON    0x90
#define MIDI_MASK_NOTEOFF   0x80
#define MIDI_MASK_NOTE      0x7f
#define MIDI_MASK_VELOCITY  0x7f

#define MIDI_MASK_CHANNEL_MODE_MESSAGES 0xb0
#define MIDI_MASK_ALLNOTESOFF           0x7b

#define MIDI_MASK_SYSTEM_COMMON_MESSAGES 0xf0



#endif //_MIDI_H_
