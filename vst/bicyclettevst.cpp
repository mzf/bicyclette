/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "bicyclettevst.h"
#include "bicyclettevstpreset.h"
#include "bicyclettevstparameters.h"
#include "midi.h"
#include "../synth/SynthParameters.h"
#include "../synth/BicycletteSynth.h"
#include "../synth/Oscillator.h"
#include "../synth/Filter.h"

#include <math.h>

//-----------------------------------------------------------------------------------------
// Bicyclette Vst Methods
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
BicycletteVst::BicycletteVst (audioMasterCallback audioMaster) :
    AudioEffectX (audioMaster, Parameters_numPresets, VSTParameters_numParams)
{
	// initialize programs
    this->presets = new BicycletteVstPreset[this->Parameters_numPresets];
    for (VstInt32 i = 0; i < Parameters_numMidiInputs; i++)
		this->channelPresets[i] = i;

	if (this->presets)
		setProgram (0);

	if (audioMaster)
	{
        // no inputs
		setNumInputs (0);

        // stereo output
        setNumOutputs (this->Parameters_numOutputs);

        //Tell Host io have changed
        ioChanged();

        //Misc
		canProcessReplacing ();
        canDoubleReplacing();
		isSynth ();

        // Unique identifier for the host
        setUniqueID (CCONST('B', 'i', 'C', 'y'));
	}

	//initProcess ();
	suspend ();

    //init the bicyclette synth
    synth = new BicycletteSynth(44100., this->Parameters_numMidiInputs, this->Parameters_numOutputs);

    //invalid synth buffers
    synthBuffers = NULL;

    //VST Parameters
    for(int i=0; i< VSTParameters_numParams; i++)
    {
        this->vstParameters[i] = 0;
    }
    CreateVSTParameters();

    //activate the chunk save mechanism
    programsAreChunks();
}

//-----------------------------------------------------------------------------------------
BicycletteVst::~BicycletteVst ()
{
    if (this->presets)
    {
		delete[] this->presets;
        this->presets = NULL;
    }

    DeleteSynthBuffers();
    DeleteVSTParameters();

    delete this->synth;
    this->synth = NULL;
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::CreateVSTParameters()
{
    //first delete the old ones
    DeleteVSTParameters();

    //Create each parameter as a class of VSTParameter
    this->vstParameters[VSTParameters_currentChannel]           = new VSTParameterCurrentChannel(SynthParameters_currentChannel, "Cur Chan", Parameters_numMidiInputs);

    this->vstParameters[VSTParameters_ChannelVolume]            = new VSTParameterPercentage(SynthParameters_ChannelVolume, "Chan Vol");
    this->vstParameters[VSTParameters_ChannelPanning]           = new VSTParameterRange(SynthParameters_ChannelPanning, "Chan Pan", "", -1., 1.);

    this->vstParameters[VSTParameters_EnvADSR_attackTime]       = new VSTParameterExponential(SynthParameters_EnvADSR_attackTime,  "Att Time", "s", this->synth->GetParameter(SynthParameters_EnvADSR_attackTimeMax));
    this->vstParameters[VSTParameters_EnvADSR_decayTime]        = new VSTParameterExponential(SynthParameters_EnvADSR_decayTime,   "Dec Time", "s", this->synth->GetParameter(SynthParameters_EnvADSR_decayTimeMax));
    this->vstParameters[VSTParameters_EnvADSR_releaseTime]      = new VSTParameterExponential(SynthParameters_EnvADSR_releaseTime, "Rel Time", "s", this->synth->GetParameter(SynthParameters_EnvADSR_releaseTimeMax));
    this->vstParameters[VSTParameters_EnvADSR_attackLevel]      = new VSTParameterPercentage(SynthParameters_EnvADSR_attackLevel,  "Att Lvl");
    this->vstParameters[VSTParameters_EnvADSR_sustainLevel]     = new VSTParameterPercentage(SynthParameters_EnvADSR_sustainLevel, "Sus Lvl");

    this->vstParameters[VSTParameters_FilterActivation]         = new VSTParameterOnOff(SynthParameters_FilterActivation,      "Filter");
    this->vstParameters[VSTParameters_FilterType]               = new VSTParameterFilterType(SynthParameters_FilterType,       "FiltType");
    this->vstParameters[VSTParameters_FilterFrequency]          = new VSTParameterExponential(SynthParameters_FilterFrequency, "FiltFreq", "Hz", (this->sampleRate * 0.5) - 10.); //max = nyquist frequency
    this->vstParameters[VSTParameters_FilterQuality]            = new VSTParameterExponential(SynthParameters_FilterQuality,   "Filter Q", "", 100.);

    this->vstParameters[VSTParameters_NoteOscModType]           = new VSTParameterNoteOscModType(SynthParameters_Note_OscModType, "O2-3 Mod");

    this->vstParameters[VSTParameters_Osc1Type]                 = new VSTParameterOscType(SynthParameters_OscType,          "O1 Type");
    this->vstParameters[VSTParameters_Osc1Square_pwmFactor]     = new VSTParameter(SynthParameters_OscSquare_pwmFactor,     "O1 PWM");
    this->vstParameters[VSTParameters_Osc1Detune]               = new VSTParameterRange(SynthParameters_OscDetune,          "O1 Tune", "", -12, 12);
    this->vstParameters[VSTParameters_Osc1Volume]               = new VSTParameterPercentage(SynthParameters_OscVolume,     "O1 Vol");
    this->vstParameters[VSTParameters_Osc2Type]                 = new VSTParameterOscType(SynthParameters_OscType,          "O2 Type");
    this->vstParameters[VSTParameters_Osc2Square_pwmFactor]     = new VSTParameter(SynthParameters_OscSquare_pwmFactor,     "O2 PWM");
    this->vstParameters[VSTParameters_Osc2Detune]               = new VSTParameterRange(SynthParameters_OscDetune,          "O2 Tune", "", -12, 12);
    this->vstParameters[VSTParameters_Osc2Volume]               = new VSTParameterPercentage(SynthParameters_OscVolume,     "O2 Vol");
    this->vstParameters[VSTParameters_Osc3Type]                 = new VSTParameterOscType(SynthParameters_OscType,          "O3 Type");
    this->vstParameters[VSTParameters_Osc3Square_pwmFactor]     = new VSTParameter(SynthParameters_OscSquare_pwmFactor,     "O3 PWM");
    this->vstParameters[VSTParameters_Osc3Detune]               = new VSTParameterRange(SynthParameters_OscDetune,          "O3 Tune", "", -12, 12);
    this->vstParameters[VSTParameters_Osc3Volume]               = new VSTParameterPercentage(SynthParameters_OscVolume,     "O3 Vol");


    //Some parameters change other parameters values, so we need to update the whole
    //GUI to synchronize the values.
    //For example when you change the channel, you want to display the new channel parameters,
    //not the old ones.
    this->vstParameters[VSTParameters_currentChannel]->SetNeedUpdateDisplay(true);
    this->vstParameters[VSTParameters_FilterType]->SetNeedUpdateDisplay(true);
    this->vstParameters[VSTParameters_Osc1Type]->SetNeedUpdateDisplay(true);
    this->vstParameters[VSTParameters_Osc2Type]->SetNeedUpdateDisplay(true);
    this->vstParameters[VSTParameters_Osc3Type]->SetNeedUpdateDisplay(true);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::DeleteVSTParameters()
{
    for(int i=0; i < VSTParameters_numParams; i++)
    {
        delete this->vstParameters[i];
        this->vstParameters[i] = 0;
    }
}

//------------------------------------------------------------------------------------------
void BicycletteVst::setProgram (VstInt32 program)
{
    //check invalid input
    if (program < 0 || program >= this->Parameters_numPresets)
		return;

    //get the corresponding preset
    /*
    BicycletteVstPreset *ap = &(this->presets[program]);
	curProgram = program;

    //TODO: update the synth with this preset
	fWaveform1 = ap->fWaveform1;
	fFreq1     = ap->fFreq1;
	fVolume1   = ap->fVolume1;

	fWaveform2 = ap->fWaveform2;
	fFreq2     = ap->fFreq2;
	fVolume2   = ap->fVolume2;

	fVolume    = ap->fVolume;
    */
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::setProgramName (char* name)
{
    this->presets[curProgram].SetName(name);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::getProgramName (char* name)
{
	vst_strncpy (name, this->presets[curProgram].GetName(), kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::getParameterLabel (VstInt32 index, char* label)
{
    if((index >= VSTParameters_numParams) || (this->vstParameters[index] == 0))
    {
        return;
    }

    this->vstParameters[index]->GetLabel(label);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::getParameterDisplay (VstInt32 index, char* text)
{
	if((index >= VSTParameters_numParams) || (this->vstParameters[index] == 0))
    {
        return;
    }

	//for oscillator parameters we first need to inform the synth which
    //oscillator we will use.
    SetSynthOscillatorIndex(index);

    int synthParamId = this->vstParameters[index]->GetSynthParamId();
    this->vstParameters[index]->GetDisplayValue(this->synth->GetParameter(synthParamId), text);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::getParameterName (VstInt32 index, char* label)
{
    if((index >= VSTParameters_numParams) || (this->vstParameters[index] == 0))
    {
        return;
    }

    this->vstParameters[index]->GetName(label);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::setParameter (VstInt32 index, float value)
{
    if((index >= VSTParameters_numParams) || (this->vstParameters[index] == 0))
    {
        return;
    }

	//for oscillator parameters we first need to inform the synth which
    //oscillator we will use.
    SetSynthOscillatorIndex(index);

    //get the associated synth param id
    int synthParamId = this->vstParameters[index]->GetSynthParamId();

    //transform the value from [0..1] to the synth range
    double synthValue = this->vstParameters[index]->GetValueToSynth(static_cast<double>(value));

    //send it to the synth
    this->synth->SetParameter(synthParamId, synthValue);

    //update graphic values
    if(this->vstParameters[index]->NeedUpdateDisplay())
    {
        this->updateDisplay();
    }
}

//-----------------------------------------------------------------------------------------
float BicycletteVst::getParameter (VstInt32 index)
{
    if((index >= VSTParameters_numParams) || (this->vstParameters[index] == 0))
    {
        return 0.f;
    }

	//for oscillator parameters we first need to inform the synth which
    //oscillator we will use.
    SetSynthOscillatorIndex(index);

    //get the associated synth parameter id and request the value
    int synthParamId = this->vstParameters[index]->GetSynthParamId();
    double synthValue = this->synth->GetParameter(synthParamId);

    //transform the synth value to match the range [0..1]
    float value = static_cast<float>(this->vstParameters[index]->GetValueFromSynth(synthValue));

    return value;
}

//-----------------------------------------------------------------------------------------
bool BicycletteVst::getOutputProperties (VstInt32 index, VstPinProperties* properties)
{
    if ((index >=0) && (index < this->Parameters_numOutputs))
	{
        //create a label "bicyclette xxx" where xxx is the index+1
        //(because index starts with 0)
		vst_strncpy (properties->label, "Bicyclette ", 63);
		char temp[11] = {0};
		int2string (index + 1, temp, 10);
		vst_strncat (properties->label, temp, 63);

        //set output flags depending on speaker configuration
		properties->flags = kVstPinIsActive;

        // mono, only index 0
		if((this->Parameters_numOutputs == 1) && (index == 0))
		{
			properties->arrangementType = kSpeakerArrMono;
		}
        // stereo, indexes 0 & 1
		else if ((this->Parameters_numOutputs == 2) && (index < 2))
		{
			properties->flags |= kVstPinIsStereo;
			properties->arrangementType = kSpeakerArrStereo;
		}
        // multichannel, not tested
		else if ((this->Parameters_numOutputs > 2) && index >= 3)
		{
			properties->flags |= kVstPinUseSpeaker;
            properties->arrangementType = kSpeakerArrUserDefined; //kSpeakerArr51;
            //TODO:
			// for old VST Host < 2.3, make 5.1 to stereo/mono/mono/stereo (L R C Lfe Ls Rs)
		}
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool BicycletteVst::getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text)
{
    if (index < this->Parameters_numPresets)
	{
        vst_strncpy (text, this->presets[index].GetName(), kVstMaxProgNameLen);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool BicycletteVst::getEffectName (char* name)
{
    vst_strncpy (name, BICYCLETTE_VST_NAME, kVstMaxEffectNameLen);
	return true;
}

//-----------------------------------------------------------------------------------------
bool BicycletteVst::getVendorString (char* text)
{
    vst_strncpy (text, BICYCLETTE_VST_VENDOR, kVstMaxVendorStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
bool BicycletteVst::getProductString (char* text)
{
    vst_strncpy (text, BICYCLETTE_VST_PRODUCT_NAME, kVstMaxProductStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::getVendorVersion ()
{
	return BICYCLETTE_VST_VERSION;
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::canDo (char* text)
{
    //from the vst sdk 2.4 doc, available "PlugCanDos" are:
    /*
	    "sendVstEvents";        ///< plug-in will send Vst events to Host
	    "sendVstMidiEvent";     ///< plug-in will send MIDI events to Host
	    "receiveVstEvents";     ///< plug-in can receive MIDI events from Host
	    "receiveVstMidiEvent";  ///< plug-in can receive MIDI events from Host
	    "receiveVstTimeInfo";   ///< plug-in can receive Time info from Host
	    "offline";              ///< plug-in supports offline functions (#offlineNotify, #offlinePrepare, #offlineRun)
	    "midiProgramNames";     ///< plug-in supports function #getMidiProgramName ()
	    "bypass";               ///< plug-in supports function #setBypass ()
    */
	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
	return -1;	// explicitly can't do; 0 => don't know
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::getNumMidiInputChannels ()
{
    return this->Parameters_numMidiInputs;
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::getNumMidiOutputChannels ()
{
	return 0; // no MIDI output back to Host app
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::setSampleRate (float sampleRate)
{
	AudioEffectX::setSampleRate (sampleRate);

	//TODO: Set bicyclette synth sample rate
    this->synth->SetParameter(SynthParameters_sampleRate, static_cast<double>(sampleRate));
}

//-----------------------------------------------------------------------------------------
// This is called by the Host,
// and tells the plug-in that the maximum block size passed to
// processReplacing() will be blockSize.
//-----------------------------------------------------------------------------------------
void BicycletteVst::setBlockSize (VstInt32 blockSize)
{
	AudioEffectX::setBlockSize (blockSize);

    //create buffers for the bicyclette synth
    CreateSynthBuffers(blockSize);
}

//-----------------------------------------------------------------------------------------
// The audio generation
//-----------------------------------------------------------------------------------------
void BicycletteVst::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
{
    //the synth works with "double", so we use a "double" buffer
    //and then cast it to floats

    //security
    if(this->synthBuffers == NULL)
    {
        CreateSynthBuffers(sampleFrames);
    }

    //render with bicyclette synth
    this->synth->Render(this->synthBuffers, sampleFrames);

    //cast to float
    for(unsigned int sampleIndex=0; sampleIndex<(unsigned int)sampleFrames; sampleIndex++)
    {
        //copy the value to outputs
        for(unsigned int outputIndex=0; outputIndex<this->Parameters_numOutputs; outputIndex++)
        {
            outputs[outputIndex][sampleIndex] = static_cast<float>(synthBuffers[outputIndex][sampleIndex]);
        }
    }
}

//-----------------------------------------------------------------------------------------
// 64 bits process replacing
//-----------------------------------------------------------------------------------------
void BicycletteVst::processDoubleReplacing(double **inputs, double **outputs, VstInt32 sampleFrames)
{
    this->synth->Render(outputs, sampleFrames);
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::processEvents (VstEvents* ev)
{
	for (VstInt32 i = 0; i < ev->numEvents; i++)
	{
        //discard events which are not Midi
		if ((ev->events[i])->type != kVstMidiType)
			continue;

        //cast the event to a Midi event
		VstMidiEvent* midiEvent = reinterpret_cast<VstMidiEvent*>(ev->events[i]);
        if(midiEvent == NULL)
            continue;

        //get channel and status code
		char* midiData = midiEvent->midiData;
        VstInt32 status = midiData[0] & MIDI_MASK_STATUS;
        VstInt32 channel = midiData[0] & MIDI_MASK_CHANNEL;
        if(status == MIDI_MASK_SYSTEM_COMMON_MESSAGES)
            continue;
        if((channel < 0) || (channel >= this->Parameters_numMidiInputs))
            continue;

        // check the status
        VstInt32 note, velocity;
        switch(status)
        {
        case MIDI_MASK_NOTEON:
            note = midiData[1] & MIDI_MASK_NOTE;
			velocity = midiData[2] & MIDI_MASK_VELOCITY;
            //some hosts send NoteOn with 0 velocity to mean NoteOff
            if(velocity == 0)
            {
                NoteOff(channel, note, midiEvent->deltaFrames);
            }
            else
            {
                NoteOn(channel, note, velocity, midiEvent->deltaFrames);
            }
            break;
        case MIDI_MASK_NOTEOFF:
            note = midiData[1] & MIDI_MASK_NOTE;
            NoteOff(channel, note, midiEvent->deltaFrames);
            break;
        case MIDI_MASK_CHANNEL_MODE_MESSAGES:
            //all notes off
			if (midiData[1] == 0x7e || midiData[1] == MIDI_MASK_ALLNOTESOFF)
				AllNotesOff(channel, midiEvent->deltaFrames);
            break;
		}
	}

	return 1;
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::CreateSynthBuffers(unsigned int size)
{
    DeleteSynthBuffers();

    this->synthBuffers = new double*[this->Parameters_numOutputs];
    for(unsigned int i=0; i<this->Parameters_numOutputs; i++)
    {
        double* channel = new double[size];
        this->synthBuffers[i] = channel;
    }
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::DeleteSynthBuffers()
{
    if(this->synthBuffers == NULL)
        return;

    for(unsigned int i=0;i<this->Parameters_numOutputs;i++)
    {
        delete[] this->synthBuffers[i];
        this->synthBuffers[i] = NULL;
    }
    delete[] this->synthBuffers;
    this->synthBuffers = NULL;
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::NoteOn(int channel, int note, int velocity, int deltaFrames)
{
    BicycletteEvent bicycletteEvent;
    bicycletteEvent.type = BicycletteEventType_NoteOn;
    bicycletteEvent.note = note;
    bicycletteEvent.channel = channel;
    bicycletteEvent.velocity = velocity;
    bicycletteEvent.delta = deltaFrames;

    this->synth->AddEvent(bicycletteEvent);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::NoteOff(int channel, int note, int deltaFrames)
{
    BicycletteEvent bicycletteEvent;
    bicycletteEvent.type = BicycletteEventType_NoteOff;
    bicycletteEvent.note = note;
    bicycletteEvent.channel = channel;
    bicycletteEvent.delta = deltaFrames;

    this->synth->AddEvent(bicycletteEvent);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::AllNotesOff(int channel, int deltaFrames)
{
    BicycletteEvent bicycletteEvent;
    bicycletteEvent.type = BicycletteEventType_AllNotesOff;
    bicycletteEvent.channel = channel;
    bicycletteEvent.delta = deltaFrames;

    this->synth->AddEvent(bicycletteEvent);
}

//-----------------------------------------------------------------------------------------
void BicycletteVst::SetSynthOscillatorIndex(int VSTparameter)
{
    //for oscillators, first check the oscillator index and inform the synth
    double index = -1.;
	switch(VSTparameter)
	{
        case VSTParameters_Osc1Type:
        case VSTParameters_Osc1Detune:
        case VSTParameters_Osc1Square_pwmFactor:
        case VSTParameters_Osc1Volume:
            index = 0.;
            break;
        case VSTParameters_Osc2Type:
        case VSTParameters_Osc2Detune:
        case VSTParameters_Osc2Square_pwmFactor:
        case VSTParameters_Osc2Volume:
            index = 1.;
            break;
        case VSTParameters_Osc3Type:
        case VSTParameters_Osc3Detune:
        case VSTParameters_Osc3Square_pwmFactor:
        case VSTParameters_Osc3Volume:
            index = 2.;
            break;
	}

	if(index >= 0.)
    {
        this->synth->SetParameter(SynthParameters_OscCurrent, index);
    }
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::getChunk(void** data, bool isPreset)
{
    //clean up the inner stream
    this->serializationStream.Clear();

    //serialize the synth
    this->synth->Serialize(serializationStream);

    //get the data as an array
    unsigned int byteSize = 0;
    serializationStream.GetData(data, &byteSize);

    return byteSize;
}

//-----------------------------------------------------------------------------------------
VstInt32 BicycletteVst::setChunk(void* data, VstInt32 byteSize, bool isPreset)
{
    //create a stream with input data
    Stream dataStream;
    dataStream.Init(data, static_cast<unsigned int>(byteSize));

    //Deserialize the synth
    this->synth->Deserialize(dataStream);

    return 0; //?
}
