/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _VST_PARAMETER_H_
#define _VST_PARAMETER_H_

#include <string>

//for vst_strncpy and vst_strncat functions
#include "vstsdk2.4/public.sdk/source/vst2.x/audioeffect.h"

//convert a double in a string representation
//see implementation in vstparameter.cpp
void double2string (double value, char* text, VstInt32 maxLen);
void int2string (VstInt32 value, char* text, VstInt32 maxLen);

//----------------------------------------------------------------
// VST Parameter class that could be derived to customize display
//----------------------------------------------------------------

class VSTParameter
{
public:

    //------------------------------------------------------------
    // Constructor with default parameters
    VSTParameter(int synthParamId, const char* name, const char* label = "") :
        name(name),
        label(label),
        synthParameterId(synthParamId),
        needUpdateDisplay(false)
        {}

    //------------------------------------------------------------
    // Empty destructor
    virtual ~VSTParameter() {}

    //------------------------------------------------------------
    // Accessors

    //Name: usually does not change
    virtual void GetName(char* outText)
    {
        vst_strncpy(outText, this->name.c_str(), kVstMaxParamStrLen);
    }

    //Display: nice string representation of the original value
    virtual void GetDisplayValue(double synthValue, char* outText)
    {
        outText[0] = 0;
        double2string(synthValue, outText, kVstMaxParamStrLen);
    }

    //Label: the sign after the parameter value (eg. "%" or "s" or "Hz")
    virtual void GetLabel(char* outText)
    {
        vst_strncpy(outText, this->label.c_str(), kVstMaxParamStrLen);
    }

    //Transform the value from synth range to vst range [0..1]
    virtual float GetValueFromSynth(double synthValue)
    {
        return static_cast<float>(synthValue);
    }

    //Transform the value from the vst range [0..1] to the custom synth range
    virtual double GetValueToSynth(double vstValue)
    {
        return vstValue;
    }

    //Get the stored synth param id
    virtual int GetSynthParamId()
    {
        return this->synthParameterId;
    }

    //Returns true if the GUI must be updated after a parameter change.
    //Returns false otherwise.
    virtual bool NeedUpdateDisplay()
    {
        return this->needUpdateDisplay;
    }

    //Set if the GUI need to be updated after a parameter change.
    virtual void SetNeedUpdateDisplay(bool value)
    {
        this->needUpdateDisplay = value;
    }

protected:

    //------------------------------------------------------------
    // Members

    std::string     name;
    std::string     label;
    int             synthParameterId;
    bool            needUpdateDisplay;
};

#endif // _VST_PARAMETER_H_
