/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _BICYCLETTE_VST_PARAMETERS_H_
#define _BICYCLETTE_VST_PARAMETERS_H_

#include "vstparameter.h"
#include "../synth/Filter.h"
#include "../synth/Oscillator.h"

double  GetExponentialParameterValue(double input, double max, bool inverse);

//----------------------------------------------
// VSTParameterPercentage
// This class displays a percentage of the value
//----------------------------------------------
class VSTParameterPercentage : public VSTParameter
{
public:
    VSTParameterPercentage(int synthParamId, const char* name):
        VSTParameter(synthParamId, name, "%")
        { }

    //override the display value to show a percentage
    void GetDisplayValue(double synthValue, char* outText)
    {
        outText[0] = 0;
        double2string(synthValue * 100, outText, kVstMaxParamStrLen);
    }

};

//----------------------------------------------
// VSTParameterOnOff
// This class displays a ON/OFF switch
//----------------------------------------------
class VSTParameterOnOff : public VSTParameter
{
public:
    VSTParameterOnOff(int synthParamId, const char* name):
        VSTParameter(synthParamId, name)
        { }

    //override the display value to show a On/Off switch
    void GetDisplayValue(double synthValue, char* outText)
    {
        if(synthValue < 0.5)
        {
            vst_strncpy (outText, "OFF", kVstMaxParamStrLen);
        }
        else
        {
            vst_strncpy (outText, "ON", kVstMaxParamStrLen);
        }
    }

};

//----------------------------------------------
// VSTParameterCurrentChannel
//----------------------------------------------
class VSTParameterCurrentChannel : public VSTParameter
{
public:
    VSTParameterCurrentChannel(int synthParamId, const char* name, int param_numMidiInputs):
        VSTParameter(synthParamId, name, "#")
        {
            if(param_numMidiInputs <=0)
            {
                this->numMidiInputs = 1.;
            }
            else
            {
                this->numMidiInputs = static_cast<double>(param_numMidiInputs);
            }
        }

    //override the display value to show a number that start from 1 instead of 0
    void GetDisplayValue(double synthValue, char* outText)
    {
        outText[0] = 0;
        int2string(static_cast<int>(synthValue) + 1, outText, kVstMaxParamStrLen);
    }

    float GetValueFromSynth(double synthValue)
    {
        //the value is between 0 and (numMidiInputs-1)
        //convert it to 0..1
        synthValue /= this->numMidiInputs;
        synthValue += 0.000001; //to be sure to be in the right interval
        return static_cast<float>(synthValue);
    }

    double GetValueToSynth(double vstValue)
    {
        //from [0..1] to [1..numMidiInputs]
        return vstValue * this->numMidiInputs;
    }

protected:
    double numMidiInputs;
};

//----------------------------------------------
// VSTParameterRange
// convert from [0..1] to a custom synth range
//----------------------------------------------
class VSTParameterRange : public VSTParameter
{
public:
    VSTParameterRange(int synthParamId, const char* name, const char* label, double _rangeMin, double _rangeMax) :
        VSTParameter(synthParamId, name, label)
    {
        //check for errors
        if(_rangeMin > _rangeMax)
        {
            this->rangeMax = _rangeMin;
            this->rangeMin = _rangeMax;
        }
        else
        {
            this->rangeMax = _rangeMax;
            this->rangeMin = _rangeMin;

            //security to avoid division by zero
            if(_rangeMin == _rangeMax)
            {
                this->rangeMax += 0.000001;
            }
        }
    }

    float GetValueFromSynth(double synthValue)
    {
        //convert from [rangeMin..rangeMax] to [0..1]
        return static_cast<float>((synthValue - this->rangeMin) / (this->rangeMax - this->rangeMin));
    }

    double GetValueToSynth(double vstValue)
    {
        //convert from [0..1] to [rangeMin..rangeMax]
        return this->rangeMin + (this->rangeMax - this->rangeMin) * vstValue;
    }

protected:
    double rangeMin;
    double rangeMax;
};

//----------------------------------------------
// VSTParameterExponential
// convert from [0..1] to nice exponential values
//----------------------------------------------
class VSTParameterExponential : public VSTParameter
{
public:
    VSTParameterExponential(int synthParamId, const char* name, const char* label, double _max) :
        VSTParameter(synthParamId, name, label)
    {
        if(_max < 0.)
        {
            _max = -_max;
        }
        this->max = _max;
    }

    float GetValueFromSynth(double synthValue)
    {
        //convert from [0..max] to [0..1]
        return static_cast<float>(GetExponentialParameterValue(synthValue, this->max, true));
    }

    double GetValueToSynth(double vstValue)
    {
        //convert from [0..1] to [0..max]
        return GetExponentialParameterValue(vstValue, this->max, false);
    }

protected:
    double max;
};

//----------------------------------------------
// VSTParameterFilterType
//----------------------------------------------
class VSTParameterFilterType : public VSTParameter
{
public:
    VSTParameterFilterType(int synthParamId, const char* name) :
        VSTParameter(synthParamId, name)
    { }

    //The display value depends on the type of the filter
    void GetDisplayValue(double synthValue, char* outText)
    {
        int filterType = static_cast<int>(synthValue);
        switch(filterType)
        {
        case FilterType_BandPass:
            vst_strncpy (outText, "BandPass", kVstMaxParamStrLen);
            break;
        case FilterType_HighPass:
            vst_strncpy (outText, "HighPass", kVstMaxParamStrLen);
            break;
        case FilterType_LowPass:
            vst_strncpy (outText, "Low Pass", kVstMaxParamStrLen);
            break;
        default:
            outText[0] = 0;
            break;
        }
    }

    float GetValueFromSynth(double synthValue)
    {
        synthValue /= static_cast<double>(FilterType_numTypes);
        synthValue += 0.000001; //to be sure to be in the right interval
        return static_cast<float>(synthValue);
    }

    //Transform the value from the vst range [0..1] to the custom synth range
    double GetValueToSynth(double vstValue)
    {
        return vstValue * static_cast<double>(FilterType_numTypes);
    }
};

//----------------------------------------------
// VSTParameterNoteOscModType
//----------------------------------------------
class VSTParameterNoteOscModType : public VSTParameter
{
public:
    VSTParameterNoteOscModType(int synthParamId, const char* name) :
        VSTParameter(synthParamId, name)
    { }

    //The display value depends on the type of the filter
    void GetDisplayValue(double synthValue, char* outText)
    {
        int modType = static_cast<int>(synthValue);
        switch(modType)
        {
        case OscillatorModulation_Add:
            vst_strncpy (outText, "Add", kVstMaxParamStrLen);
            break;
        case OscillatorModulation_FM:
            vst_strncpy (outText, "FM", kVstMaxParamStrLen);
            break;
        case OscillatorModulation_Ring:
            vst_strncpy (outText, "Ring", kVstMaxParamStrLen);
            break;
        }
    }

    float GetValueFromSynth(double synthValue)
    {
        synthValue /= static_cast<double>(OscillatorModulation_NumMod);
        synthValue += 0.000001; //to be sure to be in the right interval
        return static_cast<float>(synthValue);
    }

    //Transform the value from the vst range [0..1] to the custom synth range
    double GetValueToSynth(double vstValue)
    {
        return vstValue * static_cast<double>(OscillatorModulation_NumMod);
    }
};

//----------------------------------------------
// VSTParameterOscType
//----------------------------------------------
class VSTParameterOscType : public VSTParameter
{
public:
    VSTParameterOscType(int synthParamId, const char* name) :
        VSTParameter(synthParamId, name)
    { }

    //The display value depends on the type of the filter
    void GetDisplayValue(double synthValue, char* outText)
    {
        int oscType = static_cast<int>(synthValue);
        switch(oscType)
        {
        case OscillatorType_Saw:
            vst_strncpy (outText, "Saw", kVstMaxParamStrLen);
            break;
        case OscillatorType_Square:
            vst_strncpy (outText, "Square", kVstMaxParamStrLen);
            break;
        case OscillatorType_Sinus:
            vst_strncpy (outText, "Sinus", kVstMaxParamStrLen);
            break;
        case OscillatorType_Noise:
            vst_strncpy (outText, "Noise", kVstMaxParamStrLen);
            break;
        }
    }

    float GetValueFromSynth(double synthValue)
    {
        synthValue /= static_cast<double>(OscillatorType_numTypes);
        synthValue += 0.000001; //to be sure to be in the right interval
        return static_cast<float>(synthValue);
    }

    //Transform the value from the vst range [0..1] to the custom synth range
    double GetValueToSynth(double vstValue)
    {
        return vstValue * static_cast<double>(OscillatorType_numTypes);
    }
};

#endif // _BICYCLETTE_VST_PARAMETERS_H_
