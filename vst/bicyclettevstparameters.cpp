/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "bicyclettevstparameters.h"
#include <math.h>

//-----------------------------------------------------------------------------------------
// Create an exponential response for some parameters
//-----------------------------------------------------------------------------------------
double GetExponentialParameterValue(double input, double max, bool inverse)
{
    //check input parameters
    if((max <= 0.) || (input < 0.) || (input > 1.))
    {
        return 0.;
    }

    if(inverse == false)
    {
        //from [0..1] to [0..max]
        return pow(max + 1, input) - 1;
    }
    else
    {
        //from [0..max] to [0..1]
        return log(input + 1) / log(max + 1);
    }
}



