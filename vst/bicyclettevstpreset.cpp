/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "bicyclettevstpreset.h"

//-----------------------------------
//BicycletteVstPreset Constructor
//-----------------------------------
//Just create an empty preset
BicycletteVstPreset::BicycletteVstPreset()
{
    //init the default preset
	vst_strncpy (name, "Basic", kVstMaxProgNameLen);

    //TODO: init preset values
}

//-----------------------------------
// Name accessors
//-----------------------------------
const char* BicycletteVstPreset::GetName()
{
    return this->name;
}

void BicycletteVstPreset::SetName(char* name)
{
    //use the vst strncpy function takes care of null terminator.
    vst_strncpy (this->name, name, kVstMaxProgNameLen);
}