/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef __BICYCLETTEVST_H__
#define __BICYCLETTEVST_H__

//-----------------------------------
//The VST side of bicyclette
//This is an implementation of the VST AudioEffectX class.
//-----------------------------------

#include "vstsdk2.4/public.sdk/source/vst2.x/audioeffectx.h"

#include "../tools/Stream.h"

//forward declarations to avoid to include the .h files
class BicycletteVstPreset;
class BicycletteSynth;
class VSTParameter;

//-----------------------------------
// Constants
//-----------------------------------

// Official Bicyclette Vst Name
#define BICYCLETTE_VST_NAME         "BicycletteVst"

// Official Bicyclette Vst Developper
#define BICYCLETTE_VST_VENDOR       "Bicyclette Team"

// Official Bicyclette Product Name
#define BICYCLETTE_VST_PRODUCT_NAME "Bicyclette Vst"

// Version of the Vst
#define BICYCLETTE_VST_VERSION      1000

//-----------------------------------
// The BicycletteVst class
//-----------------------------------
class BicycletteVst : public AudioEffectX
{
    //-----------------------------------
    // Parameters for the Vst
    //-----------------------------------
    enum Parameters
    {
	    // Global
	    Parameters_numPresets       = 128,
	    Parameters_numOutputs       = 2,
        Parameters_numMidiInputs    = 16, //TODO: bind this to the channel number of the synth

	    Parameters_numParams
    };

    enum VSTParameters
    {
        VSTParameters_currentChannel,

        VSTParameters_ChannelVolume,
        VSTParameters_ChannelPanning,

        VSTParameters_EnvADSR_attackTime,
        VSTParameters_EnvADSR_attackLevel,
        VSTParameters_EnvADSR_decayTime,
        VSTParameters_EnvADSR_sustainLevel,
        VSTParameters_EnvADSR_releaseTime,

        VSTParameters_FilterActivation,
        VSTParameters_FilterType,
        VSTParameters_FilterFrequency,
        VSTParameters_FilterQuality,

        VSTParameters_NoteOscModType,

        VSTParameters_Osc1Type,
        VSTParameters_Osc1Volume,
        VSTParameters_Osc1Detune,
        VSTParameters_Osc1Square_pwmFactor,
        VSTParameters_Osc2Type,
        VSTParameters_Osc2Volume,
        VSTParameters_Osc2Detune,
        VSTParameters_Osc2Square_pwmFactor,
        VSTParameters_Osc3Type,
        VSTParameters_Osc3Volume,
        VSTParameters_Osc3Detune,
        VSTParameters_Osc3Square_pwmFactor,

        //keep it the last field, for automatic parameters count
        VSTParameters_numParams
    };

public:

    //-----------------------------------
	BicycletteVst (audioMasterCallback audioMaster);
	~BicycletteVst ();

    //-----------------------------------
    // Audio generation
    //-----------------------------------
	virtual void        processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);
    virtual void        processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames);
	virtual VstInt32    processEvents (VstEvents* events);

    //-----------------------------------
    // Preset logic (program = preset)
    //-----------------------------------
	virtual void        setProgram (VstInt32 program);
	virtual void        setProgramName (char* name);
	virtual void        getProgramName (char* name);
	virtual bool        getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text);

    //-----------------------------------
    // Parameters logic
    //-----------------------------------
	virtual void        setParameter (VstInt32 index, float value);
	virtual float       getParameter (VstInt32 index);
	virtual void        getParameterLabel (VstInt32 index, char* label);
	virtual void        getParameterDisplay (VstInt32 index, char* text);
	virtual void        getParameterName (VstInt32 index, char* text);

    //-----------------------------------
	virtual void        setSampleRate (float sampleRate);
	virtual void        setBlockSize (VstInt32 blockSize);

    //-----------------------------------
	virtual bool        getOutputProperties (VstInt32 index, VstPinProperties* properties);

    //-----------------------------------
    // PlugIn Informations Accessors
    //-----------------------------------
	virtual bool        getEffectName (char* name);
	virtual bool        getVendorString (char* text);
	virtual bool        getProductString (char* text);
	virtual VstInt32    getVendorVersion ();
	virtual VstInt32    canDo (char* text);

    //-----------------------------------
	virtual VstInt32    getNumMidiInputChannels ();
	virtual VstInt32    getNumMidiOutputChannels ();

	//-----------------------------------
	virtual VstInt32 getChunk (void** data, bool isPreset = false);
	virtual VstInt32 setChunk (void* data, VstInt32 byteSize, bool isPreset = false);

    //-----------------------------------
    // Midi programs, not implemented
    //-----------------------------------
	/*virtual VstInt32    getMidiProgramName (VstInt32 channel, MidiProgramName* midiProgramName);
	virtual VstInt32    getCurrentMidiProgram (VstInt32 channel, MidiProgramName* currentProgram);
	virtual VstInt32    getMidiProgramCategory (VstInt32 channel, MidiProgramCategory* category);
	virtual bool        hasMidiProgramsChanged (VstInt32 channel);
	virtual bool        getMidiKeyName (VstInt32 channel, MidiKeyName* keyName);*/

private:

    //-----------------------------------
    // Methods
    //-----------------------------------

    //Create the synthBuffers with correct size according to outputChannelsNumber
    void CreateSynthBuffers(unsigned int size);
    void DeleteSynthBuffers();

    //triggers notes
    void NoteOn(int channel, int note, int velocity, int deltaFrames);
    void NoteOff(int channel, int note, int deltaFrames);
    void AllNotesOff(int channel, int deltaFrames);

    //parameters management
    void    CreateVSTParameters();
    void    DeleteVSTParameters();
    void    SetSynthOscillatorIndex(int VSTparameter);

    //-----------------------------------
    // Members
    //-----------------------------------

    // double buffers for processing the synth
    double**                synthBuffers;

    //List of available presets
    BicycletteVstPreset*    presets;

    //Stores the preset associated to a channel
	VstInt32                channelPresets[Parameters_numMidiInputs];

    //The synth itself
    BicycletteSynth*        synth;

    //VST parameters array
    VSTParameter*           vstParameters[VSTParameters_numParams];

    //Stream for serialization in  GetChunk/SetChunk
    Stream                  serializationStream;

};

#endif //__BICYCLETTEVST_H__
