/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "vstparameter.h"

#include <math.h>

//----------------------------------------------------------------
// double2string function
// Original code from float2string in audioeffect.cpp
// add comments for clarity, fix some errors and code redundancies
//----------------------------------------------------------------

char GetASCIICharFromDigit(int digit)
{
    if((digit<0) ||(digit>9))
        return 0;

    //use the value of '0' as a reference
    return ((VstInt32)digit + '0');
}

//the function
void double2string (double value, char* text, VstInt32 maxLen)
{
    //constants
	double ten = 10.;
	char hugeMessage[6] = {'H', 'u', 'g', 'e', '!', 0};

	VstInt32 charCounter = 0;
	bool negative = false;
	char string[32];
	char* stringPointer;
	double integ, mantissa, m10;

    //check the validity of the value
	if (value < 0)
	{
		negative = true;
		value = -value;
		charCounter++; //for the minus char '-'
		if (value > 999999.)
		{
		    //overflow
			vst_strncpy (text, hugeMessage, maxLen);
			return;
		}
	}
	else if (value > 9999999.)
	{
	    //overflow
		vst_strncpy (text, hugeMessage, maxLen);
		return;
	}

    //put a trailing 0 at the end of the string
	stringPointer = string + 31;
	*stringPointer-- = 0;

    //---------------
    // Integral Part
    //---------------

	//set the dot for integral values (eg 123.)
	*stringPointer-- = '.';
	charCounter++;

    //write the integral digits
	integ = floor (value);
	do
	{
	    //get the last digit
		double i10 = fmod (integ, ten);

		//write the ASCII value in the string
		*stringPointer-- = GetASCIICharFromDigit(i10);

		//next digit
		integ /= ten;
		charCounter++;
	}
	while ((integ >= 1.) && (charCounter < 8));

    //add minus sign for negatives values
	if (negative)
		*stringPointer-- = '-';

    //copy our string to the output
	vst_strncpy (text, stringPointer + 1, maxLen);

	//if we already fill all the available chars, skip the floating part
	if (charCounter >= 8)
		return;

    //---------------
    // Floating Part
    //---------------

    //get only the floating part
	mantissa = fmod (value, 1.);

	//put it in the integral part
	mantissa *= pow (ten, (double)(8 - charCounter));

	while (charCounter < 8)
	{
		if (mantissa <= 0)
        {
            *stringPointer-- = '0';
        }
		else
		{
		    //get the last digit
			m10 = fmod (mantissa, ten);

			//write the ASCII value in the string
			*stringPointer-- = GetASCIICharFromDigit(m10);

			//next digit
			mantissa /= ten;
		}
		charCounter++;
	}

	//concatenate the integral part and the floating part
	vst_strncat (text, stringPointer + 1, maxLen);
}

//int2string from AudioEffect class
void int2string (VstInt32 value, char* text, VstInt32 maxLen)
{
	if (value >= 100000000)
	{
		vst_strncpy (text, "Huge!", maxLen);
		return;
	}

	if (value < 0)
	{
		vst_strncpy (text, "-", maxLen);
		value = -value;
	}
	else
		vst_strncpy (text, "", maxLen);

	bool state = false;
	for(VstInt32 div = 100000000; div >= 1; div /= 10)
	{
		VstInt32 digit = value / div;
		value -= digit * div;
		if (state || digit > 0)
		{
			char temp[2] = {static_cast<char>('0' + digit), '\0'};
			vst_strncat (text, temp, maxLen);
			state = true;
		}
	}
}
