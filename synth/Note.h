/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _NOTE_H_
#define _NOTE_H_

#define OSCILLATORS_NUMBER 3

//---------------------------------
// Forward declarations of members
//---------------------------------
class Oscillator;
class Envelope;
class Stream;

//---------------------------------
// The Note Class
//---------------------------------
class Note
{
public:

    Note();
    ~Note();

    bool    IsActive();
    void    Activate(int midiNote, double volume);
    void    Deactivate();
    void    TriggerNoteOff();

    int     GetNoteIndex();
    double  GetVolume();

    double  GetSample();

    void    SetParameter(int parameterId, double value);
    double  GetParameter(int parameterId);

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

private:

    //Disallow copy of this object
    Note&  operator = (const Note& other);
    Note(const Note& other);

    //Methods
    double  GetNotePitch(double midiNote);
    void    UpdateNotePitch();
    void    ChangeOscillatorType(unsigned int oscIndex, int type);

    //Members
    int     midiNoteIndex;
    double  currentPitch[OSCILLATORS_NUMBER];
    double  oscDetune[OSCILLATORS_NUMBER];
    double  volume;
    bool    active;

    Envelope*           envelope;

    //oscillators
    Oscillator*             oscillators[OSCILLATORS_NUMBER];
    double                  sampleRate;
    unsigned int            currentOscIndex;
    int                     oscillatorsModulationType;
};

#endif //_NOTE_H_
