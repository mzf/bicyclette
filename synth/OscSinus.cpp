/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "OscSinus.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

#include <math.h>
#define PI2 (M_PI * 2.) //6.28318530718

OscSinus::OscSinus() :
        Oscillator(OscillatorType_Sinus),
        frequency(440.),
        phase(0.),
        phaseIncrement(0.),
        sampleRate(44100.),
        volume(0.5),
        FMAmount(0.)
{
}


OscSinus::~OscSinus(void)
{
}

void OscSinus::SetParameter(int parameterEnum, double value)
{
    switch(parameterEnum)
    {
    case SynthParameters_sampleRate:
        this->sampleRate = value;
        UpdatePhaseIncrement();
        break;
    case SynthParameters_OscFrequency:
        this->frequency = value;
        UpdatePhaseIncrement();
        break;
    case SynthParameters_OscVolume:
        if((value >= 0.) && (value <=1.))
        {
            this->volume = value;
        }
        break;
    case SynthParameters_OscFMInput:
        if((value >= 0.) && (value <=1.))
        {
            this->FMAmount = value;
            UpdatePhaseIncrement();
        }
        break;
    }
}

double OscSinus::GetParameter(int parameterEnum)
{
    double value = 0.;

    switch(parameterEnum)
    {
        case SynthParameters_sampleRate:
            value = this->sampleRate;
            break;
        case SynthParameters_OscFrequency:
            value = this->frequency;
            break;
        case SynthParameters_OscVolume:
            value = this->volume;
            break;
        case SynthParameters_OscFMInput:
            value = this->FMAmount;
            break;
    }

    return value;
}


double OscSinus::GetSample()
{
    //simple sinus
    double val = sin(this->phase);

    //update phase
    this->phase += this->phaseIncrement;
    if(this->phase > PI2)
        this->phase -= PI2;


    return val*this->volume;
}

void OscSinus::ResetPhase()
{
    this->phase = 0.;
}

void OscSinus::UpdatePhaseIncrement()
{
    //update the phase according to the frequency, eventually with modulation
    this->phaseIncrement = (this->frequency + (OSC_FM_MAX_DELTA * this->FMAmount)) * PI2 / this->sampleRate;
}

//Serialization
const char OscSinus::serializationVersion = 1;

void OscSinus::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->frequency);
    stream.Write(this->phase);
    stream.Write(this->sampleRate);
    stream.Write(this->volume);
    stream.Write(this->FMAmount);
}

void OscSinus::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->frequency);
    stream.Read(this->phase);
    stream.Read(this->sampleRate);
    stream.Read(this->volume);
    stream.Read(this->FMAmount);

    this->UpdatePhaseIncrement();
}
