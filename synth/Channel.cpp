/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "Channel.h"
#include "SynthParameters.h"
#include "FilterLowPass.h"
#include "FilterHighPass.h"
#include "FilterBandPass.h"
#include "../tools/Stream.h"

Channel::Channel(void) :
    panning(0.),
    volume(1.),
    filter(0),
    sampleRate(44100.)
{
    //create the filter
    this->ChangeFilterType(FilterType_BandPass);
}

Channel::~Channel(void)
{
}

void Channel::NoteOn(int midiNoteIndex, int velocity)
{
    unsigned int newNoteIndex = this->GetAnEmptyNote(midiNoteIndex);

    //convert velocity from [0 127] to [0.0 1.0]
    double doubleVelocity = static_cast<double>(velocity)/127.0;

    this->notes[newNoteIndex].Activate(midiNoteIndex, doubleVelocity);
}

void Channel::NoteOff(int noteIndex)
{
    //search for this note
    for(unsigned int i = 0; i < MAX_NOTES_PER_CHANNEL; i++)
    {
        if(this->notes[i].GetNoteIndex() == noteIndex)
        {
            //shut down the note
            this->notes[i].TriggerNoteOff();

            //we found a note, so don't process next ones
            return; //break;
        }
    }
}

void Channel::AllNotesOff()
{
    for(unsigned int i = 0; i < MAX_NOTES_PER_CHANNEL; i++)
    {
        //shut down the note
        this->notes[i].Deactivate();
    }
}

void Channel::RenderStereo(double *left, double *right)
{
    double val = 0.;

    //Get values for notes
    for(unsigned int i= 0; i< MAX_NOTES_PER_CHANNEL; i++)
    {
        if(this->notes[i].IsActive())
        {
            val += this->notes[i].GetSample();
        }
    }

    //apply filter
    val = this->filter->GetSample(val);

    //Apply panning (and channel volume):
    // positive value -> right
    // negative value -> left
    // panning is between -1 and 1
    *right = val * (0.5 + 0.5 * this->panning) * this->volume;
    *left = val * (0.5 - 0.5 * this->panning) * this->volume;

}

unsigned int Channel::GetAnEmptyNote(int futureNoteIndex)
{
    bool foundEmpty = false;
    bool foundSimilar = false;
    unsigned int result, resultEmpty, resultSimilar;

    //Parse the available notes to find inactives ones
    //and if the note is already played
    for(unsigned int i= 0; i< MAX_NOTES_PER_CHANNEL; i++)
    {
        if((foundEmpty == false) &&
           (this->notes[i].IsActive() == false))
        {
            foundEmpty = true;
            resultEmpty = i;
        }

        if((foundSimilar == false) &&
           (this->notes[i].GetNoteIndex() == futureNoteIndex))
        {
            foundSimilar = true;
            resultSimilar = i;
        }
    }

    //First pick a note already played
    if(foundSimilar)
    {
        result = resultSimilar;
    }
    //Second case: an empty note
    else if(foundEmpty)
    {
        result = resultEmpty;
    }
    //Last case: nothing available
    else
    {
        //get the one with the lowest volume
        //(advice from http://blog.kebby.org/?p=40)
        double volume = 2.0;
        for(unsigned int i= 0; i< MAX_NOTES_PER_CHANNEL; i++)
        {
            if(this->notes[i].GetVolume() < volume)
            {
                result = i;
                volume = this->notes[i].GetVolume();
            }
        }
    }

    return result;
}

void Channel::SetParameter(int parameterId, double value)
{
    switch(parameterId)
    {
    case SynthParameters_ChannelPanning:
        if((value <= 1.) && (value >= -1.))
        {
            this->panning = value;
        }
        break;
    case SynthParameters_ChannelVolume:
        if((value <= 1.) && (value >= 0.))
        {
            this->volume = value;
        }
        break;

    case SynthParameters_FilterActivation:
        if(value < 0.5)
        {
            this->filter->SetActiveState(false);
        }
        else
        {
            this->filter->SetActiveState(true);
        }
        break;
    case SynthParameters_FilterType:
        this->ChangeFilterType(static_cast<int>(value));
        break;
    case SynthParameters_FilterFrequency:
    case SynthParameters_FilterQuality:
        this->filter->SetParameter(parameterId, value);
        break;

    case SynthParameters_sampleRate:
        this->sampleRate = value;
        this->filter->SetParameter(parameterId, value);
        SetAllNotesParameter(SynthParameters_sampleRate, value);
        break;

        //Forward these parameters to the channel's notes:
    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_sustainLevel:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_OscType:
    case SynthParameters_OscSquare_pwmFactor:
    case SynthParameters_OscDetune:
    case SynthParameters_OscVolume:
    case SynthParameters_OscCurrent:
    case SynthParameters_Note_OscModType:
        SetAllNotesParameter(parameterId, value);
        break;
    }
}

double Channel::GetParameter(int parameterId)
{
    double value = 0.;

    switch(parameterId)
    {
    case SynthParameters_ChannelPanning:
        value = this->panning;
        break;
    case SynthParameters_ChannelVolume:
        value = this->volume;
        break;
    case SynthParameters_sampleRate:
        value = this->sampleRate;
        break;

    case SynthParameters_FilterActivation:
        if(this->filter->GetActiveState() == true)
        {
            value = 1.;
        }
        else
        {
            value = 0.;
        }
        break;
    case SynthParameters_FilterType:
        value = this->filter->GetType();
        break;
    case SynthParameters_FilterFrequency:
    case SynthParameters_FilterFrequencyMax:
    case SynthParameters_FilterQuality:
        value = this->filter->GetParameter(parameterId);
        break;

    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_sustainLevel:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_EnvADSR_attackTimeMax:
    case SynthParameters_EnvADSR_decayTimeMax:
    case SynthParameters_EnvADSR_releaseTimeMax:
    case SynthParameters_OscType:
    case SynthParameters_OscSquare_pwmFactor:
    case SynthParameters_OscDetune:
    case SynthParameters_OscVolume:
    case SynthParameters_OscCurrent:
    case SynthParameters_Note_OscModType:
        value = this->notes[0].GetParameter(parameterId);
        break;
    }

    return value;
}

void Channel::SetAllNotesParameter(int parameterId, double value)
{
    for(int i=0; i<MAX_NOTES_PER_CHANNEL; i++)
    {
            this->notes[i].SetParameter(parameterId, value);
    }
}

void Channel::ChangeFilterType(int filterType)
{
    //check for valid parameters
    if((filterType < 0) || (filterType >= FilterType_numTypes))
    {
        return;
    }

    //if the type is the same, we change nothing
    if((this->filter != 0) && (this->filter->GetType() == filterType))
    {
        return;
    }

    //delete the current oscillator
    delete this->filter;
    this->filter = 0;

    switch(filterType)
    {
    case FilterType_BandPass:
        this->filter = new FilterBandPass();
        break;
    case FilterType_HighPass:
        this->filter = new FilterHighPass();
        break;
    case FilterType_LowPass:
        this->filter = new FilterLowPass();
        break;
    default:
        //assert here?
        this->filter = new FilterLowPass();
        break;
    }

    //update samplerate
    this->filter->SetParameter(SynthParameters_sampleRate, this->sampleRate);
}

//Serialization
const char Channel::serializationVersion = 1;

void Channel::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);
    stream.Write(this->panning);
    stream.Write(this->volume);
    stream.Write(this->sampleRate);

    //Filter
    stream.Write(static_cast<int>(this->filter->GetType()));
    stream.Write(this->filter->GetActiveState());
    this->filter->Serialize(stream);

    //Notes
    for(unsigned int i = 0; i< MAX_NOTES_PER_CHANNEL; i++)
    {
        this->notes[i].Serialize(stream);
    }
}

void Channel::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);
    stream.Read(this->panning);
    stream.Read(this->volume);
    stream.Read(this->sampleRate);

    //Filter type
    int filterType;
    stream.Read(filterType);
    this->ChangeFilterType(filterType);

    //Once the filter has been created, set its state
    //and deserialize it.
    bool filterActiveState;
    stream.Read(filterActiveState);
    this->filter->SetActiveState(filterActiveState);
    this->filter->Deserialize(stream);

    //Notes
    for(unsigned int i = 0; i< MAX_NOTES_PER_CHANNEL; i++)
    {
        this->notes[i].Deserialize(stream);
    }

}
