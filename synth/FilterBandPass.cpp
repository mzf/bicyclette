/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "FilterBandPass.h"
#include "SynthParameters.h"

#include <math.h>

FilterBandPass::FilterBandPass() :
    FilterIIR(FilterType_BandPass)
{
    this->UpdateFilterCoefficients();
}

FilterBandPass::~FilterBandPass()
{

}


void    FilterBandPass::UpdateFilterCoefficients()
{
    //source: http://freeverb3.sourceforge.net/iir_filter.shtml

    //H(S) = (S/Q)/(S^2 + S/Q + 1)

    double W = tan(M_PI * this->frequency / this->sampleRate);
    double N = 1./(W*W + W/this->quality + 1.);

    //coefficients
    this->a0 = N*W/this->quality;
    this->a1 = 0.;
    this->a2 = -a0;
    this->b1 = 2. * N * ( W*W - 1.);
    this->b2 = N * (W*W - W/this->quality + 1.);

}
