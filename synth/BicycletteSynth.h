/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _BICYCLETTESYNTH_H_
#define _BICYCLETTESYNTH_H_

#include "renderer.h"

#include "BicycletteEvent.h"
#include "../tools/LinkedList.h"
#include "../tools/Stream.h"

//---------------------------------
// Forward declarations of members
//---------------------------------
class Channel;

//---------------------------------
// Bicyclette Event Linked List
//---------------------------------
typedef LinkedList<BicycletteEvent> BicycletteEventList;


//---------------------------------
// The synthesizer class
//---------------------------------
class BicycletteSynth :
    public Renderer
{

public:

    BicycletteSynth(double sampleRate, unsigned int inputChannelsNumber, unsigned int outputChannelsNumber);
    ~BicycletteSynth(void);

    void    Render(double** outputs, unsigned int sampleFrames);
    void    SetParameter(int parameterId, double value);
    double  GetParameter(int parameterId);

    void AddEvent(BicycletteEvent& incommingEvent);

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

    void                GetPatch(Stream& stream);
    void                SetPatch(Stream& stream);

private:

    //-----------------------------------
    // Methods
    //-----------------------------------

    void ProcessEvents();

    void InitChannels();

    //-----------------------------------
    // Members
    //-----------------------------------

    double              sampleRate;
    unsigned int        inputChannelsNumber;
    unsigned int        outputChannelsNumber;
    unsigned long       tickCount;

    BicycletteEventList eventList;
    Channel*            channels;
    unsigned int        currentChannel;

};

#endif //_BICYCLETTESYNTH_H_
