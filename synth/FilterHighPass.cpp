/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "FilterHighPass.h"

#include <math.h>

FilterHighPass::FilterHighPass() :
    FilterIIR(FilterType_HighPass)
{
    this->UpdateFilterCoefficients();
}

FilterHighPass::~FilterHighPass()
{

}

void FilterHighPass::UpdateFilterCoefficients()
{
    //source: http://freeverb3.sourceforge.net/iir_filter.shtml

    //H(S) = S^2/(S^2 + S/Q + 1)

    double W = tan(M_PI * this->frequency / this->sampleRate);
    double N = 1./(W*W + W/this->quality + 1.);

    //coefficients
    this->a0 = N;
    this->a1 = -2. * N;
    this->a2 = a0;
    this->b1 = 2. * N * ( W*W - 1.);
    this->b2 = N * (W*W - W/this->quality + 1.);

}
