/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _SYNTH_PARAMETERS_H_
#define _SYNTH_PARAMETERS_H_

//----------------------------------
// Parameters of the synth
//----------------------------------
enum SynthParameters
{
    SynthParameters_sampleRate,
    SynthParameters_inputChannelsNumber, //Midi inputs
    SynthParameters_outputChannelsNumber, //audio outputs

    SynthParameters_currentChannel,

    SynthParameters_ChannelPanning,
    SynthParameters_ChannelVolume,

    SynthParameters_EnvADSR_attackTime,
    SynthParameters_EnvADSR_attackLevel,
    SynthParameters_EnvADSR_decayTime,
    SynthParameters_EnvADSR_sustainLevel,
    SynthParameters_EnvADSR_releaseTime,
    SynthParameters_EnvADSR_attackTimeMax,
    SynthParameters_EnvADSR_decayTimeMax,
    SynthParameters_EnvADSR_releaseTimeMax,

    SynthParameters_FilterActivation,
    SynthParameters_FilterType,
    SynthParameters_FilterFrequency,
    SynthParameters_FilterFrequencyMax,
    SynthParameters_FilterQuality,

    SynthParameters_Note_OscModType,

    SynthParameters_OscCurrent,
    SynthParameters_OscFrequency,
    SynthParameters_OscDetune,
    SynthParameters_OscType,
    SynthParameters_OscVolume,
    SynthParameters_OscFMInput,
    SynthParameters_OscSquare_pwmFactor,

};

#endif //_SYNTH_PARAMETERS_H_
