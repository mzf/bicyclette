/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "FilterIIR.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

FilterIIR::FilterIIR(FilterType type) :
    Filter(type),
    sampleRate(44100.),
    frequency(4000.),
    quality(1.),
    a0(0.),
    a1(0.),
    a2(0.),
    b1(0.),
    b2(0.),
    out_t1(0.),
    out_t2(0.),
    in_t1(0.),
    in_t2(0.)
{
}

FilterIIR::~FilterIIR()
{

}

double  FilterIIR::GetSample(double input)
{
    if(this->activated == false)
    {
        //bypass the filter
        return input;
    }

    double output = this->a0 * input +
                    this->a1 * this->in_t1 +
                    this->a2 * this->in_t2 -
                    this->b1 * this->out_t1 -
                    this->b2 * this->out_t2;


    //update the buffers
    this->out_t2 = this->out_t1;
    this->out_t1 = output;
    this->in_t2 = this->in_t1;
    this->in_t1 = input;

    return output;
}

void    FilterIIR::SetParameter(int parameterEnum, double value)
{
    switch(parameterEnum)
    {
    case SynthParameters_sampleRate:
        this->sampleRate = value;
        this->UpdateFilterCoefficients();
        break;
    case SynthParameters_FilterFrequency:
        this->frequency = value;
        this->UpdateFilterCoefficients();
        break;
    case SynthParameters_FilterQuality:
        this->quality = value;
        if(this->quality < 0.001)
        {
            this->quality = 0.001;
        }
        this->UpdateFilterCoefficients();
        break;
    }
}

double  FilterIIR::GetParameter(int parameterEnum)
{
    double value = 0.;

    switch(parameterEnum)
    {
    case SynthParameters_sampleRate:
        value = this->sampleRate;
        break;
    case SynthParameters_FilterFrequency:
        value = this->frequency;
        break;
    case SynthParameters_FilterQuality:
        value = this->quality;
        break;
    case SynthParameters_FilterFrequencyMax:
        //the nyquist frequency
        value = this->sampleRate * 0.5;
        break;
    }

    return value;
}

//Serialization
const char FilterIIR::serializationVersion = 1;

void FilterIIR::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->sampleRate);
    stream.Write(this->frequency);
    stream.Write(this->quality);
}

void FilterIIR::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->sampleRate);
    stream.Read(this->frequency);
    stream.Read(this->quality);

    this->UpdateFilterCoefficients();
    this->out_t1 = 0.;
    this->out_t2 = 0.;
    this->in_t1 = 0.;
    this->in_t2 = 0.;
}
