/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _OSCILLATOR_H_
#define _OSCILLATOR_H_

#define OSC_FM_MAX_DELTA 100.

//---------------------------------
// Oscillators types
//---------------------------------
enum OscillatorType
{
    OscillatorType_Sinus,
    OscillatorType_Square,
    OscillatorType_Saw,
    OscillatorType_Noise,

    //always the last to keep a count up-to-date
    OscillatorType_numTypes
};

//---------------------------------
// Oscillators Modulation Types
//---------------------------------
enum OscillatorModulation
{
    OscillatorModulation_Add,
    OscillatorModulation_FM,
    OscillatorModulation_Ring,

    //always the last to keep a count up-to-date
    OscillatorModulation_NumMod
};

//Forward Declaration
class Stream;

//---------------------------------
// The oscillator interface
//---------------------------------
class Oscillator
{
public:
    Oscillator(OscillatorType type)
    {
        this->type = type;
    }
    virtual ~Oscillator() {};

    virtual double  GetSample() = 0;
    virtual void    SetParameter(int parameterEnum, double value) = 0;
    virtual double  GetParameter(int parameterEnum) = 0;
    virtual void    ResetPhase() = 0;

    OscillatorType  GetType()
    {
        return this->type;
    };

    //Serialization
    virtual void Serialize(Stream& stream) = 0;
    virtual void Deserialize(Stream& stream) = 0;

protected:
    OscillatorType type;
};

#endif //_OSCILLATOR_H_
