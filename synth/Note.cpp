/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "Note.h"
#include "OscSinus.h"
#include "OscSquare.h"
#include "OscSaw.h"
#include "OscNoise.h"
#include "EnvADSR.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

#include <math.h>

Note::Note(void) :
    midiNoteIndex(0),
    volume(0.),
    active(false),
    sampleRate(44100.),
    currentOscIndex(0),
    oscillatorsModulationType(OscillatorModulation_Add)
{
    this->envelope = new EnvADSR(this->sampleRate);

    //create our oscillators
    for(int i=0; i<OSCILLATORS_NUMBER; i++)
    {
        this->oscillators[i] = new OscSinus();
        this->oscillators[i]->SetParameter(SynthParameters_sampleRate, this->sampleRate);
        this->currentPitch[i] = 0.;
        this->oscDetune[i] = 0.;
    }
}

Note::~Note(void)
{
    delete envelope;

    for(int i=0; i<OSCILLATORS_NUMBER; i++)
    {
        delete this->oscillators[i];
        this->oscillators[i] = 0;
    }
}

bool Note::IsActive()
{
    return this->active;
}

void Note::Activate(int midiNote, double volume)
{
    //set the note parameters
    this->midiNoteIndex = midiNote;
    this->UpdateNotePitch();
    this->volume = volume;
    this->active = true;

    //check if the note was already playing and activate the envelope
    bool alreadyActive = this->envelope->IsActive();
    this->envelope->KeyPressed();

    //update oscillators
    for(int i=0; i<OSCILLATORS_NUMBER; i++)
    {
        this->oscillators[i]->SetParameter(SynthParameters_OscFrequency, this->currentPitch[i]);

        //do not reset phase if the note was already playing
        if(alreadyActive == false)
        {
            this->oscillators[i]->ResetPhase();
        }
    }
}

void Note::Deactivate()
{
    this->active = false;
}

int Note::GetNoteIndex()
{
    return this->midiNoteIndex;
}

double Note::GetVolume()
{
    return this->volume;
}

double Note::GetSample()
{
    double val = 0.;

    //check envelope
    if(this->envelope->IsActive() == false)
    {
        this->Deactivate();
    }

    if(this->active == false)
    {
        return 0.;
    }

    //get oscillators values

    switch(this->oscillatorsModulationType)
    {

    //Additive synthesis
    case OscillatorModulation_Add:
        for(int i = 0; i<OSCILLATORS_NUMBER; i++)
        {
            val += this->oscillators[i]->GetSample();
        }
        break;

    //Frequency modulation synthesis
    //(only oscillators 2 and 3)
    case OscillatorModulation_FM:
        //Osc 1
        val = this->oscillators[0]->GetSample();
        //Osc 2 and 3
        this->oscillators[2]->SetParameter(SynthParameters_OscFMInput, this->oscillators[1]->GetSample());
        val += this->oscillators[2]->GetSample();
        break;

    //Ring modulation
    //(only oscillators 2 and 3)
    case OscillatorModulation_Ring:
        //Osc 1
        val = this->oscillators[0]->GetSample();
        //Osc 2 and 3
        val += this->oscillators[1]->GetSample() * this->oscillators[2]->GetSample();
        break;

    }


    //apply envelope
    val *= this->envelope->GetSample();

    //apply volume
    val *= this->volume;

    return val;
}

double Note::GetNotePitch(double midiNote)
{
    if(midiNote < 0.)
        midiNote = 0.;

    if(midiNote > 127.)
        midiNote = 127.;

    //source: http://en.wikipedia.org/wiki/MIDI_Tuning_Standard
    //A4 = 440 Hz = midi note 69
    return pow(2.0,(midiNote-69.)/12.) * 440.;
}

void Note::UpdateNotePitch()
{
    for(int i =0; i < OSCILLATORS_NUMBER; i++)
    {
        this->currentPitch[i] = this->GetNotePitch(static_cast<double>(this->midiNoteIndex) + this->oscDetune[i]);
    }
}

void Note::TriggerNoteOff()
{
    this->envelope->KeyReleased();
}

void Note::SetParameter(int parameterId, double value)
{
    unsigned int index;

    switch(parameterId)
    {
    case SynthParameters_sampleRate:
        this->sampleRate = value;
        for(int i = 0; i<OSCILLATORS_NUMBER; i++)
        {
            this->oscillators[i]->SetParameter(SynthParameters_sampleRate, this->sampleRate);
        }
        break;

    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_EnvADSR_sustainLevel:
        this->envelope->SetParameter(parameterId, value);
        break;

    case SynthParameters_Note_OscModType:
        index = static_cast<unsigned int>(value);
        if(index < OscillatorModulation_NumMod)
        {
            this->oscillatorsModulationType = index;
        }
        break;

    case SynthParameters_OscCurrent:
        index = static_cast<unsigned int>(value);
        if(index < OSCILLATORS_NUMBER)
        {
            this->currentOscIndex = index;
        }
        break;
    case SynthParameters_OscType:
        this->ChangeOscillatorType(this->currentOscIndex, static_cast<int>(value));
        break;
    case SynthParameters_OscDetune:
        this->oscDetune[this->currentOscIndex] = value;
        this->UpdateNotePitch();
        this->oscillators[this->currentOscIndex]->SetParameter(SynthParameters_OscFrequency, this->currentPitch[this->currentOscIndex]);
        break;

    //Forward these parameters to the right oscillator
    case SynthParameters_OscVolume:
    case SynthParameters_OscSquare_pwmFactor:
        this->oscillators[this->currentOscIndex]->SetParameter(parameterId, value);
        break;
    }
}

double Note::GetParameter(int parameterId)
{
    double value = 0.;
    switch(parameterId)
    {
    case SynthParameters_sampleRate:
        value = this->sampleRate;
        break;

    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_EnvADSR_sustainLevel:
    case SynthParameters_EnvADSR_attackTimeMax:
    case SynthParameters_EnvADSR_decayTimeMax:
    case SynthParameters_EnvADSR_releaseTimeMax:
        value = this->envelope->GetParameter(parameterId);
        break;

    case SynthParameters_Note_OscModType:
        value = static_cast<double>(this->oscillatorsModulationType);
        break;
    case SynthParameters_OscCurrent:
        value = this->currentOscIndex;
        break;
    case SynthParameters_OscType:
        value = static_cast<double>(this->oscillators[this->currentOscIndex]->GetType());
        break;
    case SynthParameters_OscDetune:
        value = this->oscDetune[this->currentOscIndex];
        break;
    //Get these parameters from the right oscillator
    case SynthParameters_OscSquare_pwmFactor:
    case SynthParameters_OscVolume:
        value = this->oscillators[this->currentOscIndex]->GetParameter(parameterId);
        break;
    }
    return value;
}

void Note::ChangeOscillatorType(unsigned int oscIndex, int type)
{
    //check for valid parameters
    if((type < 0) || (type >= OscillatorType_numTypes) ||
       (oscIndex >= OSCILLATORS_NUMBER))
    {
        return;
    }

    //if the type is the same, we change nothing
    if(this->oscillators[oscIndex]->GetType() == type)
    {
        return;
    }

    //delete the current oscillator
    delete this->oscillators[oscIndex];
    this->oscillators[oscIndex] = 0;

    switch(type)
    {
    case OscillatorType_Saw:
        this->oscillators[oscIndex] = new OscSaw();
        break;
    case OscillatorType_Sinus:
        this->oscillators[oscIndex] = new OscSinus();
        break;
    case OscillatorType_Square:
        this->oscillators[oscIndex] = new OscSquare();
        break;
    case OscillatorType_Noise:
        this->oscillators[oscIndex] = new OscNoise();
        break;
    default:
        //assert here?
        this->oscillators[oscIndex] = new OscNoise();
        break;
    }

    //update samplerate
    this->oscillators[oscIndex]->SetParameter(SynthParameters_sampleRate, this->sampleRate);

}

//Serialization
const char Note::serializationVersion = 1;

void Note::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->midiNoteIndex);
    stream.Write(this->volume);
    stream.Write(this->active);
    stream.Write(this->sampleRate);
    stream.Write(this->currentOscIndex);
    stream.Write(this->oscillatorsModulationType);

    for(int i=0; i<OSCILLATORS_NUMBER; i++)
    {
        stream.Write(this->currentPitch[i]);
        stream.Write(this->oscDetune[i]);
        stream.Write(static_cast<int>(this->oscillators[i]->GetType()));
        this->oscillators[i]->Serialize(stream);
    }

    this->envelope->Serialize(stream);
}

void Note::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->midiNoteIndex);
    stream.Read(this->volume);
    stream.Read(this->active);
    stream.Read(this->sampleRate);
    stream.Read(this->currentOscIndex);
    stream.Read(this->oscillatorsModulationType);

    for(int i=0; i<OSCILLATORS_NUMBER; i++)
    {
        stream.Read(this->currentPitch[i]);
        stream.Read(this->oscDetune[i]);
        int oscType;
        stream.Read(oscType);
        this->ChangeOscillatorType(i, oscType);
        this->oscillators[i]->Deserialize(stream);
    }

    this->envelope->Deserialize(stream);
}
