/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "OscNoise.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

#include <stdlib.h>     // srand, rand
#include <time.h>       // time

OscNoise::OscNoise() :
    Oscillator(OscillatorType_Noise),
    volume(0.5)
{
    srand (time(NULL));
}


OscNoise::~OscNoise(void)
{
}

void OscNoise::SetParameter(int parameterEnum, double value)
{
    switch(parameterEnum)
    {
    case SynthParameters_OscVolume:
        if((value >= 0.) && (value <=1.))
        {
            this->volume = value;
        }
        break;
    }
}

double OscNoise::GetParameter(int parameterEnum)
{
    double value = 0.;
    switch(parameterEnum)
    {
    case SynthParameters_OscVolume:
            value = this->volume;
            break;
    }

    return value;
}

double OscNoise::GetSample()
{
    //simple random Noise
    double val = static_cast<double>(rand())/static_cast<double>(RAND_MAX);

    //transform it from [0..1] to [-1..1]
    val = (val * 2.) - 1.;

    return val*this->volume;
}

void OscNoise::ResetPhase()
{
}

//Serialization
const char OscNoise::serializationVersion = 1;

void OscNoise::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->volume);
}

void OscNoise::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->volume);
}
