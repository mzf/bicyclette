/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _FILTER_LOWPASS_H_
#define _FILTER_LOWPASS_H_

#include "FilterIIR.h"

class FilterLowPass : public FilterIIR
{
public:
    FilterLowPass();
    ~FilterLowPass();

protected:

    //methods
    void    UpdateFilterCoefficients();

};

#endif // _FILTER_LOWPASS_H_
