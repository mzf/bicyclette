/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#include "Note.h"

#define MAX_NOTES_PER_CHANNEL 32

//---------------------------------
// Forward declarations of members
//---------------------------------
class Filter;
class Stream;

//---------------------------------
// The Channel Class
//---------------------------------
class Channel
{
public:

    Channel(void);
    ~Channel(void);

    void NoteOn(int noteIndex, int velocity);
    void NoteOff(int noteIndex);
    void AllNotesOff();

    void RenderStereo(double* left, double* right);

    void    SetParameter(int parameterId, double value);
    double  GetParameter(int parameterId);

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

private:

    //Methods
    unsigned int    GetAnEmptyNote(int futureNoteIndex);
    void            SetAllNotesParameter(int parameterId, double value);
    void            ChangeFilterType(int type);

    //Members
    Note            notes[MAX_NOTES_PER_CHANNEL];
    double          panning;
    double          volume;
    Filter*         filter;
    double          sampleRate;
};

#endif //_CHANNEL_H_
