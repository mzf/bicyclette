/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _ENVADSR_H_
#define _ENVADSR_H_

#include "Envelope.h"

//Forward Declaration
class Stream;

//---------------------
// The EnvADSR class
//---------------------
class EnvADSR : public Envelope
{
public:

    //constructor/destructor
    EnvADSR(double sampleRate);
    ~EnvADSR();

    //Envelope interface
    double  GetSample();
    void    SetParameter(int parameterEnum, double value);
    double  GetParameter(int parameterEnum);
    void    KeyPressed();
    void    KeyReleased();
    bool    IsActive();

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

private:

    enum Status
    {
        Status_Off,
        Status_Attack,
        Status_Decay,
        Status_Sustain,
        Status_Release
    };

    double  GetPolynomialEnvelope(double startLevel, double startTime, double endLevel, double endTime, double currentTime);

    double  sampleRate;
    Status  status;

    //ADSR main parameters
    double  attackTime;
    double  attackLevel;
    double  decayTime;
    double  sustainLevel;
    double  releaseTime;

    //internal states to handle special cases
    double  attackStartLevel;
    double  releaseStartLevel;
    double  currentLevel;

    const double    attackTimeMax;
    const double    decayTimeMax;
    const double    releaseTimeMax;

    unsigned long   sampleCount;

};


#endif //_ENVADSR_H_
