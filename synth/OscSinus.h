/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _OSC_SINUS_H_
#define _OSC_SINUS_H_

#include "oscillator.h"
class OscSinus :
    public Oscillator
{

public:
    OscSinus();
    ~OscSinus();

    //Oscillator interface
    double  GetSample();
    void    SetParameter(int parameterEnum, double value);
    double  GetParameter(int parameterEnum);
    void    ResetPhase();

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

private:

    void    UpdatePhaseIncrement();

    double  frequency;
    double  phase;
    double  phaseIncrement;
    double  sampleRate;
    double  volume;
    double  FMAmount;
};

#endif //_OSC_SINUS_H_
