/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "EnvADSR.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

//the smallest envelope time
#define TINY_ENV_TIME 0.001

EnvADSR::EnvADSR(double sampleRate) :
        sampleRate(sampleRate),
        status(Status_Off),
        attackTime(0.1),
        attackLevel(1.),
        decayTime(0.2),
        sustainLevel(0.8),
        releaseTime(0.5),
        attackStartLevel(0.),
        releaseStartLevel(sustainLevel),
        currentLevel(0.),
        attackTimeMax(10.),
        decayTimeMax(10.),
        releaseTimeMax(10.),
        sampleCount(0)
{
}

EnvADSR::~EnvADSR()
{
}

void EnvADSR::KeyPressed()
{
    //set the envelope to the attach phase
    this->status = this->Status_Attack;
    this->sampleCount = 0;

    //store the current level to avoid clipping if the note was previously played
    this->attackStartLevel = this->currentLevel;
}

void EnvADSR::KeyReleased()
{
    //set the envelope to the release phase
    this->status = this->Status_Release;

    //restart the counter to measure the release time
    this->sampleCount = 0;

    //store the current level to avoid clipping if we are not in
    //the sustain state
    this->releaseStartLevel = this->currentLevel;
}

void EnvADSR::SetParameter(int parameterEnum, double value)
{
    switch(parameterEnum)
    {
    case SynthParameters_EnvADSR_attackLevel:
        if((value>=0.)&&(value<=1.))
        {
            this->attackLevel = value;
        }
        break;
    case SynthParameters_EnvADSR_attackTime:
        if((value>=0.)&&(value<=this->attackTimeMax))
        {
            this->attackTime = value;
            if(this->attackTime < TINY_ENV_TIME)
            {
                this->attackTime = TINY_ENV_TIME;
            }
        }
        break;
    case SynthParameters_EnvADSR_decayTime:
        if((value>=0.)&&(value<=this->decayTimeMax))
        {
            this->decayTime = value;
            if(this->decayTime < TINY_ENV_TIME)
            {
                this->decayTime = TINY_ENV_TIME;
            }
        }
        break;
    case SynthParameters_EnvADSR_releaseTime:
        if((value>=0.)&&(value<=this->releaseTimeMax))
        {
            this->releaseTime = value;
            if(this->releaseTime < TINY_ENV_TIME)
            {
                this->releaseTime = TINY_ENV_TIME;
            }
        }
        break;
    case SynthParameters_EnvADSR_sustainLevel:
        if((value>=0.)&&(value<=1.))
        {
            this->sustainLevel = value;
        }
        break;

    case SynthParameters_sampleRate:
        if(value>0.)
        {
            this->sampleRate = value;
        }
        break;
    }
}

double EnvADSR::GetParameter(int parameterEnum)
{
    double value = 0.;
    switch(parameterEnum)
    {
    case SynthParameters_EnvADSR_attackLevel:
        value = this->attackLevel;
        break;
    case SynthParameters_EnvADSR_attackTime:
        value = this->attackTime;
        break;
    case SynthParameters_EnvADSR_decayTime:
        value = this->decayTime;
        break;
    case SynthParameters_EnvADSR_releaseTime:
        value = this->releaseTime;
        break;
    case SynthParameters_EnvADSR_sustainLevel:
        value = this->sustainLevel;
        break;
    case SynthParameters_EnvADSR_attackTimeMax:
        value = this->attackTimeMax;
        break;
    case SynthParameters_EnvADSR_decayTimeMax:
        value = this->decayTimeMax;
        break;
    case SynthParameters_EnvADSR_releaseTimeMax:
        value = this->releaseTimeMax;
        break;

    case SynthParameters_sampleRate:
        value = this->sampleRate;
        break;
    }
    return value;
}

double EnvADSR::GetSample()
{
    double returnValue = 1.;

    //get the time in second from the start of the envelope
    double time = static_cast<double>(this->sampleCount)/this->sampleRate;

    //first update status
    switch(this->status)
    {

    case Status_Attack:
        if(time > this->attackTime)
        {
            //Attack -> Decay
            this->status = this->Status_Decay;
        }
        break;

    case Status_Decay:
        if(time > (this->attackTime + this->decayTime))
        {
            //Decay -> Sustain
            this->status = this->Status_Sustain;
        }
        break;

    case Status_Sustain:
        //nothing change here, just wait for the key to be released.
        break;

    case Status_Release:
        if(time > this->releaseTime)
        {
            //Release -> Off
            this->status = this->Status_Off;
        }
        break;

    case Status_Off:
        //no change in the status
        break;

    }

    //get the corresponding value
    //double slope = 0.; //for linear approximations
    switch(this->status)
    {

        //0 to attackLevel
    case Status_Attack:
        //linear:
        //returnValue = (time * this->attackLevel)/this->attackTime;

        //polynomial:
        returnValue = GetPolynomialEnvelope(this->attackStartLevel, 0.,
                                            this->attackLevel,      this->attackTime,
                                            time);
        break;

        //attackLevel to sustainLevel
    case Status_Decay:
        //linear:
        //slope = (this->sustainLevel - this->attackLevel)/this->decayTime;
        //returnValue = slope * time + this->attackLevel - (slope * this->attackTime);

        //polynomial:
        returnValue = GetPolynomialEnvelope(this->attackLevel,  this->attackTime,
                                            this->sustainLevel, this->attackTime + this->decayTime,
                                            time);
        break;

        //sustain, just hold the note
    case Status_Sustain:
        returnValue = this->sustainLevel;
        break;

        //release start level to 0
    case Status_Release:
        //linear:
        //returnValue = this->sustainLevel - time * (this->sustainLevel/this->releaseTime);

        //polynomial:
        returnValue = GetPolynomialEnvelope(this->releaseStartLevel, 0.,
                                            0.,                      this->releaseTime,
                                            time);
        break;

    case Status_Off:
        returnValue = 0.;
        break;
    }

    //prepare the next sample
    this->sampleCount++;

    //store the current level
    this->currentLevel = returnValue;

    return returnValue;
}

bool EnvADSR::IsActive()
{
    return (this->status != this->Status_Off);
}

double EnvADSR::GetPolynomialEnvelope(double startLevel, double startTime, double endLevel, double endTime, double currentTime)
{
    //Calculate a second order polynomial P(t) with these conditions:
    // P(startTime) = startLevel
    // P(endTime) = endLevel
    // P'(endTime) = 0 (for a smooth arrival)

    //          startLevel - endLevel
    // P(t) = ------------------------- .(t^2 - 2.endTime.t + endTime^2) + endLevel
    //         (startTime - endTime)^2

    return ((startLevel-endLevel)/((startTime-endTime)*(startTime-endTime)))*(currentTime*currentTime - 2*endTime*currentTime + endTime*endTime) + endLevel;
}

//Serialization
const char EnvADSR::serializationVersion = 1;

void EnvADSR::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->sampleRate);
    stream.Write(static_cast<int>(this->status));
    stream.Write(this->attackTime);
    stream.Write(this->attackLevel);
    stream.Write(this->decayTime);
    stream.Write(this->sustainLevel);
    stream.Write(this->releaseTime);
    stream.Write(this->attackStartLevel);
    stream.Write(this->releaseStartLevel);
    stream.Write(this->currentLevel);
    stream.Write(this->sampleCount);
}

void EnvADSR::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->sampleRate);
    int integerStatus;
    stream.Read(integerStatus);
    this->status = static_cast<Status>(integerStatus);
    stream.Read(this->attackTime);
    stream.Read(this->attackLevel);
    stream.Read(this->decayTime);
    stream.Read(this->sustainLevel);
    stream.Read(this->releaseTime);
    stream.Read(this->attackStartLevel);
    stream.Read(this->releaseStartLevel);
    stream.Read(this->currentLevel);
    stream.Read(this->sampleCount);
}
