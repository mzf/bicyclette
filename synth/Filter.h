/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _FILTER_H_
#define _FILTER_H_

//---------------------------------
// Filter types
//---------------------------------
enum FilterType
{
    FilterType_LowPass,
    FilterType_HighPass,
    FilterType_BandPass,

    //always the last to keep a count up-to-date
    FilterType_numTypes
};

//Forward declaration
class Stream;

//---------------------------------
// The filter interface
//---------------------------------
class Filter
{
public:
    Filter(FilterType type)
    {
        this->type = type;
        this->activated = false;
    }
    virtual ~Filter() {};

    virtual double  GetSample(double input) = 0;
    virtual void    SetParameter(int parameterEnum, double value) = 0;
    virtual double  GetParameter(int parameterEnum) = 0;

    FilterType  GetType()
    {
        return this->type;
    };

    bool GetActiveState()
    {
        return this->activated;
    }

    void SetActiveState(bool value)
    {
        this->activated = value;
    }

    //Serialization
    virtual void Serialize(Stream& stream) = 0;
    virtual void Deserialize(Stream& stream) = 0;

protected:
    FilterType  type;
    bool        activated;
};

#endif //_FILTER_H_
