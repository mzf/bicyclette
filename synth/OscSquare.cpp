/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "OscSquare.h"
#include "SynthParameters.h"
#include "../tools/Stream.h"

#include <math.h>


OscSquare::OscSquare() :
    Oscillator(OscillatorType_Square),
    frequency(440.),
    phase(0.),
    sampleRate(44100.),
    pwmFactor(0.5),
    volume(0.5),
    FMAmount(0.)
{
    UpdatePhaseIncrement();
}


OscSquare::~OscSquare(void)
{
}

void OscSquare::SetParameter(int parameterEnum, double value)
{
    switch(parameterEnum)
    {
    case SynthParameters_sampleRate:
        this->sampleRate = value;
        UpdatePhaseIncrement();
        break;
    case SynthParameters_OscFrequency:
        this->frequency = value;
        UpdatePhaseIncrement();
        break;
    case SynthParameters_OscSquare_pwmFactor:
        if((value >= 0.) && (value <=1.))
        {
            this->pwmFactor = value;

            //bound check
            if(this->pwmFactor > 0.99)
            {
                this->pwmFactor = 0.99;
            }
            if(this->pwmFactor < 0.01)
            {
                this->pwmFactor = 0.01;
            }
        }
        break;
    case SynthParameters_OscVolume:
        if((value >= 0.) && (value <=1.))
        {
            this->volume = value;
        }
        break;
    case SynthParameters_OscFMInput:
        if((value >= 0.) && (value <=1.))
        {
            this->FMAmount = value;
            UpdatePhaseIncrement();
        }
        break;
    }
}

double OscSquare::GetParameter(int parameterEnum)
{
    double value = 0.;

    switch(parameterEnum)
    {
        case SynthParameters_sampleRate:
            value = this->sampleRate;
            break;
        case SynthParameters_OscFrequency:
            value = this->frequency;
            break;
        case SynthParameters_OscSquare_pwmFactor:
            value = this->pwmFactor;
            break;
        case SynthParameters_OscVolume:
            value = this->volume;
            break;
        case SynthParameters_OscFMInput:
            value = this->FMAmount;
            break;
    }

    return value;
}

double OscSquare::GetSample()
{
    double val = 0.;

    // Square waveform using sinc function
    // ===================================
    //
    //            sin(Pi*x)
    // sinc(x) = -----------
    //              Pi*x
    //
    // I noticed that sinc(x^a) with high values of "a"
    // is a nice upper square waveform for x in [-1..1].
    //
    //                    ^
    //           _________|_________
    /*          /        1|         \               */
    //          |         |         |
    //          |         |         |
    //   -----------------+-------------------->
    //          -1        0         1
    //
    // So I use this waveform to generate the whole square
    // including pulse width modulation (PWM).
    //
    //    ^
    //   1| ________
    /*    |/        \                                */
    //    |         |
    //    |         |
    //   -+---------+---------------+-->
    //    |0     pwm|              1|
    //    |         |               |
    //  -1|         \_______________/
    //    |
    //

    //the exposant factor for the sinc function
    int a = 10;

    //before the "pwm" point
    if(this->phase < this->pwmFactor)
    {
        //x from [0..pwm] to [-1..1]
        double x = (2.*this->phase)/this->pwmFactor - 1.;

        //create the x^a
        for(int i =0; i<a; i++)
        {
            x *= x;
        }

        //apply the sinc function
        x *= M_PI;
        if(x == 0.) //avoid numerical error when divide by 0
        {
            val = 1.;
        }
        else
        {
            val = sin(x)/x;
        }
    }

    //after the "pwm" point
    else
    {
        //x from [pwm..1] to [-1..1]
        double x = (this->phase - this->pwmFactor)/(1. - this->pwmFactor);
        x = 2.*x-1.;

        //create the x^a
        for(int i =0; i<a; i++)
        {
            x *= x;
        }

        //apply the sinc function
        x *= M_PI;
        if(x==0.)//avoid numerical error when divide by 0
        {
            val = -1.;
        }
        else
        {
            val = -sin(x)/x; //minus sign because we are in the second part of the curve
        }

    }


    //update phase
    this->phase += this->phaseIncrement;
    if(this->phase > 1.)
        this->phase -= 1.;

    return val*this->volume;
}

void OscSquare::ResetPhase()
{
    this->phase = 0.;
}

void OscSquare::UpdatePhaseIncrement()
{
    //update the phase according to the frequency, eventually with modulation
    this->phaseIncrement = (this->frequency + (OSC_FM_MAX_DELTA * this->FMAmount)) / this->sampleRate;
}

//Serialization
const char OscSquare::serializationVersion = 1;

void OscSquare::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);

    stream.Write(this->frequency);
    stream.Write(this->phase);
    stream.Write(this->sampleRate);
    stream.Write(this->volume);
    stream.Write(this->FMAmount);
    stream.Write(this->pwmFactor);
}

void OscSquare::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);

    stream.Read(this->frequency);
    stream.Read(this->phase);
    stream.Read(this->sampleRate);
    stream.Read(this->volume);
    stream.Read(this->FMAmount);
    stream.Read(this->pwmFactor);

    this->UpdatePhaseIncrement();
}
