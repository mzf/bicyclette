/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "BicycletteSynth.h"
#include "SynthParameters.h"
#include "Channel.h"

const char BicycletteSynth::serializationVersion = 1;

BicycletteSynth::BicycletteSynth(double sampleRate, unsigned int inChannelsNumber, unsigned int outChannelsNumber):
    sampleRate(sampleRate),
    inputChannelsNumber(inChannelsNumber),
    outputChannelsNumber(outChannelsNumber),
    tickCount(0),
    channels(0),
    currentChannel(0)
{
    InitChannels();
}

BicycletteSynth::~BicycletteSynth(void)
{
    delete[] this->channels;
}

void BicycletteSynth::Render(double** outputs, unsigned int sampleFrames)
{
    double* outputBuffer = 0;

    if(this->outputChannelsNumber > 0)
    {
        outputBuffer = new double[this->outputChannelsNumber];
    }
    else //no output ?
    {
        //render nothing
        return;
    }

    for(unsigned int sampleIndex=0; sampleIndex<sampleFrames; sampleIndex++)
    {
        double attenuation = 0.5;

        //Check for new event at this sample
        ProcessEvents();

        //check channel buffers and add them
        for(unsigned int channelIndex=0; channelIndex<this->outputChannelsNumber; channelIndex++)
        {
            outputBuffer[channelIndex] = 0.;
        }
        for(unsigned int i=0; i<this->inputChannelsNumber; i++)
        {
            double leftValue = 0.;
            double rightValue = 0.;
            this->channels[i].RenderStereo(&leftValue, &rightValue);

            switch(this->outputChannelsNumber)
            {
            case 1:
                //mono, take only the left channel
                outputBuffer[0] += leftValue * attenuation;
                break;
            case 2:
                //stereo
                outputBuffer[0] += leftValue * attenuation;
                outputBuffer[1] += rightValue * attenuation;
                break;
            default:
                //multichannel not supported yet
                break;
            }
        }

        //clip the signal in [-1,1]
        for(unsigned int channelIndex=0; channelIndex<this->outputChannelsNumber; channelIndex++)
        {
            if(outputBuffer[channelIndex] > 1.0)
            {
                outputBuffer[channelIndex] = 1.0;
            }

            if(outputBuffer[channelIndex] < -1.0)
            {
                outputBuffer[channelIndex] = -1.0;
            }
        }

        //copy the value
        for(unsigned int channelIndex=0; channelIndex<this->outputChannelsNumber; channelIndex++)
        {
            outputs[channelIndex][sampleIndex] = outputBuffer[channelIndex];
        }

        //next tick
        this->tickCount++;
    }

    //clear the buffer
    delete[] outputBuffer;
}

void BicycletteSynth::SetParameter(int parameterId, double value)
{
    unsigned int uint_value;

    switch(parameterId)
    {
    case SynthParameters_sampleRate:
        this->sampleRate = value;
        //TODO: propagate sample rate change inside the synth (osc...)
        break;
    case SynthParameters_inputChannelsNumber:
        if(value > 0.)
        {
            this->inputChannelsNumber = static_cast<unsigned int>(value);
            //recreate channels
            InitChannels();
        }
        break;
    case SynthParameters_outputChannelsNumber:
        this->outputChannelsNumber = static_cast<unsigned int>(value);
        break;

    case SynthParameters_currentChannel:
        uint_value = static_cast<unsigned int>(value);
        if((uint_value >= 0) && (uint_value < this->inputChannelsNumber))
        {
            this->currentChannel = uint_value;
        }
        break;

    //Forward these parameters to the current channel:
    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_sustainLevel:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_OscType:
    case SynthParameters_OscSquare_pwmFactor:
    case SynthParameters_OscDetune:
    case SynthParameters_OscVolume:
    case SynthParameters_OscCurrent:
    case SynthParameters_Note_OscModType:
    case SynthParameters_ChannelPanning:
    case SynthParameters_ChannelVolume:
    case SynthParameters_FilterFrequency:
    case SynthParameters_FilterQuality:
    case SynthParameters_FilterType:
    case SynthParameters_FilterActivation:
        this->channels[this->currentChannel].SetParameter(parameterId, static_cast<double>(value));
        break;
    }
}

double BicycletteSynth::GetParameter(int parameterId)
{
    double value = 0.;
    switch(parameterId)
    {
    case SynthParameters_sampleRate:
        value = this->sampleRate;
        break;
    case SynthParameters_inputChannelsNumber:
        value = static_cast<double>(this->inputChannelsNumber);
        break;
    case SynthParameters_outputChannelsNumber:
        value = static_cast<double>(this->outputChannelsNumber);
        break;

    case SynthParameters_currentChannel:
        value = static_cast<double>(this->currentChannel);
        break;

    case SynthParameters_EnvADSR_attackTime:
    case SynthParameters_EnvADSR_attackLevel:
    case SynthParameters_EnvADSR_decayTime:
    case SynthParameters_EnvADSR_sustainLevel:
    case SynthParameters_EnvADSR_releaseTime:
    case SynthParameters_EnvADSR_attackTimeMax:
    case SynthParameters_EnvADSR_decayTimeMax:
    case SynthParameters_EnvADSR_releaseTimeMax:
    case SynthParameters_OscType:
    case SynthParameters_OscSquare_pwmFactor:
    case SynthParameters_OscDetune:
    case SynthParameters_OscVolume:
    case SynthParameters_OscCurrent:
    case SynthParameters_Note_OscModType:
    case SynthParameters_ChannelPanning:
    case SynthParameters_ChannelVolume:
    case SynthParameters_FilterFrequency:
    case SynthParameters_FilterFrequencyMax:
    case SynthParameters_FilterQuality:
    case SynthParameters_FilterType:
    case SynthParameters_FilterActivation:
        value = this->channels[this->currentChannel].GetParameter(parameterId);
        break;
    }
    return value;
}

void BicycletteSynth::AddEvent(BicycletteEvent &incommingEvent)
{
    this->eventList.Add(incommingEvent);
}

void BicycletteSynth::ProcessEvents()
{
    BicycletteEvent* currentEvent = eventList.Begin();
    while(currentEvent != 0)
    {
        if(currentEvent->delta <= 0)
        {
            //process the event

            //check for a valid channel
            if((currentEvent->channel >=0) &&
                ((unsigned int)(currentEvent->channel) < this->inputChannelsNumber))
            {
                switch(currentEvent->type)
                {
                case BicycletteEventType_NoteOn:
                    this->channels[currentEvent->channel].NoteOn(currentEvent->note, currentEvent->velocity);
                    break;
                case BicycletteEventType_NoteOff:
                    this->channels[currentEvent->channel].NoteOff(currentEvent->note);
                    break;
                case BicycletteEventType_AllNotesOff:
                    this->channels[currentEvent->channel].AllNotesOff();
                    break;
                }
            }

            //delete the processed event
            eventList.DeleteCurrent();
        }
        else
        {
            //decrease the delta for the next tick
            currentEvent->delta--;
        }

        currentEvent = eventList.Next();
    }
}

void BicycletteSynth::InitChannels()
{
    //delete old channels
    if(this->channels != 0)
    {
        delete[] this->channels;
        this->channels = 0;
    }

    //allocate new ones
    this->channels = new Channel[this->inputChannelsNumber];
}

void BicycletteSynth::Serialize(Stream& stream)
{
    stream.Write(this->serializationVersion);
    stream.Write(this->sampleRate);
    stream.Write(this->inputChannelsNumber);
    stream.Write(this->outputChannelsNumber);
    stream.Write(this->tickCount);
    stream.Write(this->currentChannel);

    for(unsigned int i=0; i<this->inputChannelsNumber; i++)
    {
        this->channels[i].Serialize(stream);
    }

}

void BicycletteSynth::Deserialize(Stream& stream)
{
    char version;
    stream.Read(version);
    stream.Read(this->sampleRate);
    stream.Read(this->inputChannelsNumber);
    stream.Read(this->outputChannelsNumber);
    stream.Read(this->tickCount);
    stream.Read(this->currentChannel);

    InitChannels();

    for(unsigned int i=0; i<this->inputChannelsNumber; i++)
    {
        this->channels[i].Deserialize(stream);
    }
}

void BicycletteSynth::GetPatch(Stream& stream)
{
    //TODO: Get all parameters via GetParameter method in order to create a patch
}

void BicycletteSynth::SetPatch(Stream& stream)
{
    //TODO: Update the synth with SetParameter method
}
