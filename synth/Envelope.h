/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/


#ifndef _ENVELOPE_H_
#define _ENVELOPE_H_

//Forward declaration
class Stream;

//-----------------------
//The Envelope Interface
//-----------------------
class Envelope
{
public:
    virtual ~Envelope() {};

    virtual double  GetSample() = 0;
    virtual void    SetParameter(int parameterEnum, double value) = 0;
    virtual double  GetParameter(int parameterEnum) = 0;
    virtual void    KeyPressed() = 0;
    virtual void    KeyReleased() = 0;
    virtual bool    IsActive() = 0;

    //Serialization
    virtual void    Serialize(Stream& stream) = 0;
    virtual void    Deserialize(Stream& stream) = 0;
};

#endif //_ENVELOPE_H_
