/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _FILTER_IIR_H_
#define _FILTER_IIR_H_

//This is a 2nd order IIR Filter

#include "Filter.h"

//Forward declaration
class Stream;

//------------------------
// The IIR Filter Class
//------------------------
class FilterIIR : public Filter
{
public:
    FilterIIR(FilterType type);
    ~FilterIIR();

    //Filter interface
    double  GetSample(double input);
    void    SetParameter(int parameterEnum, double value);
    double  GetParameter(int parameterEnum);

    //Serialization
    static const char   serializationVersion;
    void                Serialize(Stream& stream);
    void                Deserialize(Stream& stream);

protected:

    //methods
    virtual void UpdateFilterCoefficients() = 0;

    //current parameters
    double sampleRate;
    double frequency;
    double quality;

    //filter coefficients
    double a0;
    double a1;
    double a2;
    double b1;
    double b2;

    //output buffers
    double out_t1; //output(t-1)
    double out_t2; //output(t-2)

    //input buffers
    double in_t1; //input(t-1)
    double in_t2; //input(t-2)

};

#endif // _FILTER_IIR_H_
