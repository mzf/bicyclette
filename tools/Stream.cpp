/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#include "Stream.h"

Stream::Stream()
{

}

Stream::~Stream()
{

}

// Writers ------------------------------------------
void Stream::Write(const char& value)
{
    this->stream.write(&value, sizeof(char));
}

void Stream::Write(const int& value)
{
    this->stream.write((char*) &value, sizeof(int));
}

void Stream::Write(const bool& value)
{
    //convert it to a char
    char charValue = value ? 1 : 0;
    this->stream.write(&charValue, sizeof(char));
}

void Stream::Write(const unsigned int& value)
{
    this->stream.write((char*) &value, sizeof(unsigned int));
}

void Stream::Write(const unsigned long& value)
{
    this->stream.write((char*) &value, sizeof(unsigned long));
}

void Stream::Write(const double& value)
{
    this->stream.write((char*) &value, sizeof(double));
}

// Readers ------------------------------------------
void Stream::Read(char& value)
{
    this->stream.read(&value, sizeof(char));
    if(this->stream.good() == false)
    {
        value = 0; //default/error value
    }
}

void Stream::Read(int& value)
{
    this->stream.read((char*) &value, sizeof(int));
    if(this->stream.good() == false)
    {
        value = 0; //default/error value
    }
}

void Stream::Read(bool& value)
{
    char charValue;
    this->stream.read(&charValue, sizeof(char));
    if(this->stream.good() == false)
    {
        value = false; //default/error value
    }
    else
    {
        //conversion to bool with 0=>false other=>true
        value = (charValue != 0);
    }
}

void Stream::Read(unsigned int& value)
{
    this->stream.read((char*) &value, sizeof(unsigned int));
    if(this->stream.good() == false)
    {
        value = 0; //default/error value
    }
}

void Stream::Read(unsigned long& value)
{
    this->stream.read((char*) &value, sizeof(unsigned long));
    if(this->stream.good() == false)
    {
        value = 0; //default/error value
    }
}

void Stream::Read(double& value)
{
    this->stream.read((char*) &value, sizeof(double));
    if(this->stream.good() == false)
    {
        value = 0.0; //default/error value
    }
}

// Buffer Accessors -----------------------------------------------
void Stream::Clear()
{
    this->stream.str("");
}

void Stream::Init(void* data, unsigned int byteSize)
{
    std::string inputString((char*)data, byteSize);
    this->stream.str(inputString);
}

void Stream::GetData(void** data, unsigned int* byteSize)
{
    dataString = this->stream.str();
    *data = (void*)dataString.c_str();
    *byteSize = dataString.size();
}
