/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

//--------------------------------------------------------------------------
// Simple (and a bit dirty) linked list
//--------------------------------------------------------------------------

template <class T>
class LinkedList
{
public:

    //Constructor/Destructor
    LinkedList(): 
        first(0), 
        last(0), 
        current(0),
        currentIsNext(false)
    {

    };
    ~LinkedList()
    {
        DeleteAll();
    };

    //Add an BicycletteEvent
    void Add(T& dataToStore)
    {
        Node* newNode = new Node();

        //use the copy constructor to keep our own BicycletteEvent
        newNode->data = new T(dataToStore);
        newNode->next = 0; //append at end -> no next node
        
        if(first == 0) //empty list
        {
            //this is the first element
            newNode->previous = 0;
            this->first = newNode;
            this->last = newNode;
        }
        else
        {
            newNode->previous = this->last;
            this->last->next = newNode;

            //set it the last node
            this->last = newNode;
        }
    };

    //Delete the current node after a call to Begin() or Next()
    void DeleteCurrent()
    {
        if(this->current != 0)
        {
            //the current node after the delete
            Node* newCurrent;
            if(this->current == this->first)
            {
                newCurrent = this->current->next;
                this->currentIsNext = true;
                this->first = newCurrent;
            }
            else if(this->current == this->last)
            {
                newCurrent = this->current->previous;
                this->last = newCurrent;
            }
            else //general case: delete in the middle
            {
                newCurrent = this->current->previous;
            }

            //change links
            if(this->current->previous != 0) //not the first
            {
                this->current->previous->next = this->current->next;
            }

            if(this->current->next != 0) //not the last
            {
                this->current->next->previous = this->current->previous;
            }

            //delete objects
            delete this->current->data;
            this->current->data = 0;
            delete this->current;
            
            //update the current pointer
            this->current = newCurrent;
        }
    };

    //Delete all elements of tje list
    void DeleteAll()
    {
        //delete all nodes
        T* ev = Begin();
        while(ev != 0)
        {
            DeleteCurrent();
            ev = Next();
        }
    };

    //Iteration, only from the first to the last
    T* Begin()
    {
        if(first == 0)
        {
            return 0;
        }
        else
        {
            //init the current pointer
            this->current = first;
            currentIsNext = false;

            return this->current->data;
        }
    };
    
    T* Next()
    {
        if(this->current == 0)
        {
            return 0;
        }
        else
        {
            //if we already iterate, do not iterate
            if(currentIsNext)
            {
                currentIsNext = false;
            }
            else //iterate
            {
                this->current = this->current->next;
            }

            if(this->current == 0)
            {
                return 0;
            }
            else
            {
                return this->current->data;
            }
        }
    };

private:
    //Nested struct to handle link between elements
    struct Node
    {
        T*      data;
        Node*   next;
        Node*   previous;
    };

    //first & last node of our linked list
    Node*   first;
    Node*   last;

    //internal pointer to the current accessed node
    Node*   current;

    //internal flag: when we delete the element we iterate and
    //we move the current pointer to the next element. So it's
    //important that the method Next() do not iterate again.
    bool    currentIsNext;
};

#endif //_LINKED_LIST_H_