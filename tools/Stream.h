/***************************************************************************
    This file is a part of the Bicyclette project, an experimental
    software synthesizer.

    Copyright (C) 2013  Fran�ois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef _STREAM_H_
#define _STREAM_H_

#include <sstream>

//---------------------------------
// The Stream class
//---------------------------------
class Stream
{
public:
    Stream();
    ~Stream();

    // Buffer Accessors -----------
    void Clear();
    void Init(void* data, unsigned int byteSize);
    void GetData(void** data, unsigned int* byteSize);

    // Writers --------------------
    void Write(const char& value);
    void Write(const int& value);
    void Write(const bool& value);
    void Write(const unsigned int& value);
    void Write(const unsigned long& value);
    void Write(const double& value);

    //Readers ---------------------
    void Read(char& value);
    void Read(int& value);
    void Read(bool& value);
    void Read(unsigned int& value);
    void Read(unsigned long& value);
    void Read(double& value);

private:
    //the inner stream implementation
    std::stringstream stream;

    //the data stream for c_str persistence
    std::string dataString;
};

#endif
